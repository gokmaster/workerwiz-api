<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class Confirmer extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'confirmer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'type', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function confirmerAll($companyId) {
        return DB::table($this->table)
            ->select('confirmer.user_id', 'users.fname', 'users.lname')
            ->leftjoin('users', 'confirmer.user_id', '=', 'users.id')
            ->where('confirmer.company_id', $companyId)
            ->orderBy('users.fname', 'ASC')
            ->get();
    }

    public function userIsApprover($userId) {
        return Confirmer::where(['user_id'=> $userId, 'type'=>'approver'])->count();
    }

    public function confirmersWithUsers($companyId) {
        return DB::table($this->table)
            ->select('confirmer.id as confirmer_id', 'users.id as user_id', 'users.fname', 'users.lname', 'confirmer.type')
            ->rightjoin('users', 'confirmer.user_id', '=' , 'users.id')
            //->whereNull('team_id')
            //->orwhere('team_id', $team_id)
            ->where('users.status', 'active')
            ->where('users.company_id', $companyId)
            ->orderby('users.fname', 'ASC')
            ->get();
    }

    public function confirmersAdd($members = array(),$companyId) {
        DB::transaction(function()  use ($members,$companyId) {
            Confirmer::where('company_id', $companyId)->delete();
            Confirmer::insert($members);
        });
    }
}
