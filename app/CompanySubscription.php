<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\SubscriptionPlanOrder;
use App\Company;

class CompanySubscription extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'company_subscription';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subscription_plan_order_id', 'subscription_plan_no', 'subscription_expirydate','user_quantity', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function companySubscriptionCreate($subscriptionPlanOrderId) {
        DB::transaction(function()  use ($subscriptionPlanOrderId) {
            $subcripPlanOrder = SubscriptionPlanOrder::where('id',$subscriptionPlanOrderId)->where('paid',0)->first();
            $subcripPlanOrder = json_decode(json_encode($subcripPlanOrder),true); // convert to array

            $company = Company::where('id',$subcripPlanOrder['company_id'])->first();
            $company = json_decode(json_encode($company),true); // convert to array

            $subscripExpiryDate = $company['subscription_expirydate'];

            $latestCompanySubscrip = CompanySubscription::where('company_id',$subcripPlanOrder['company_id'])
                                        ->orderBy('subscription_expirydate','DESC')->first();
            $latestCompanySubscrip = json_decode(json_encode($latestCompanySubscrip),true); // convert to array

            if ($latestCompanySubscrip) {
                if (strtotime($subscripExpiryDate) < strtotime($latestCompanySubscrip['subscription_expirydate'])) {
                    // Set $subscripExpiryDate to the lastest subscription expiry-date of company if not already set
                    $subscripExpiryDate = $latestCompanySubscrip['subscription_expirydate'];
                }
            }

            if (strtotime($subscripExpiryDate) < strtotime('now')) { // if $subscripExpiryDate is already expired, extend expiry-date from today
                $subscripExpiryDate = date('Y-m-d H:i:s', strtotime(' + '. $subcripPlanOrder['subscription_period_days'] .' days')); // add days subscribed-for to today's date
            } else { // if $subscripExpiryDate is a future date, then extend expiry-date from that latest future expiry-date
                $subscripExpiryDate = date('Y-m-d H:i:s', strtotime($subscripExpiryDate. ' + '. $subcripPlanOrder['subscription_period_days'] .' days')); // add days subscribed-for to latest expiry date
            }

            if ($subcripPlanOrder) {
                CompanySubscription::create([
                    'subscription_plan_order_id' => $subcripPlanOrder['id'],
                    'subscription_plan_no' => $subcripPlanOrder['subscription_plan_no'],
                    'subscription_expirydate' => $subscripExpiryDate,
                    'user_quantity' => $subcripPlanOrder['user_quantity'],
                    'company_id' => $subcripPlanOrder['company_id'],
                ]);

                 // if company subscription is expired, update subscription date
                if (strtotime($company['subscription_expirydate']) < strtotime('now')) {
                    $oldestUnexpiredSubscrip =  CompanySubscription::where('company_id',$subcripPlanOrder['company_id'])
                                                ->whereDate('subscription_expirydate', '>=', Carbon::now())
                                                ->orderBy('subscription_expirydate','ASC')->first();
                    $oldestUnexpiredSubscrip = json_decode(json_encode($oldestUnexpiredSubscrip),true); // convert to array

                    // Assign subscription to company
                    Company::where('id',$subcripPlanOrder['company_id'])->update([
                        'subscription_plan_no' => $oldestUnexpiredSubscrip['subscription_plan_no'],
                        'subscription_expirydate' => $oldestUnexpiredSubscrip['subscription_expirydate'],
                        'user_quantity' => $oldestUnexpiredSubscrip['user_quantity'],
                    ]);

                    CompanySubscription::where('id',$oldestUnexpiredSubscrip['id'])->update(['used'=>1]); // Mark company-subscription as used
                }

                // Mark subscription order as Paid
                SubscriptionPlanOrder::where('id',$subscriptionPlanOrderId)->update([
                    'paid' => 1,
                    'paid_datetime' => date('Y-m-d H:i:s'),
                ]);
            }
        });
    }

    public function assignUnusedSubscriptionPlanToCompany($companyId) {
        $oldestUnusedUnexpiredSubscrip =  CompanySubscription::where('company_id',$companyId)
                                                ->where('used',0)
                                                ->whereDate('subscription_expirydate', '>=', Carbon::now())
                                                ->orderBy('subscription_expirydate','ASC')->first();
        $oldestUnusedUnexpiredSubscrip = json_decode(json_encode($oldestUnusedUnexpiredSubscrip),true); // convert to array

        if ($oldestUnusedUnexpiredSubscrip) {
            Company::where('id',$companyId)->update([
                'subscription_plan_no' => $oldestUnusedUnexpiredSubscrip['subscription_plan_no'],
                'subscription_expirydate' => $oldestUnusedUnexpiredSubscrip['subscription_expirydate'],
                'user_quantity' => $oldestUnusedUnexpiredSubscrip['user_quantity'],
            ]);

            CompanySubscription::where('id',$oldestUnusedUnexpiredSubscrip['id'])->update(['used'=>1]); // Mark company-subscription as used
            return true;
        }

        return false;
    }
}
