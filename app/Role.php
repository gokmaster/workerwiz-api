<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class Role extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'role';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_name', 'codename', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function roleCreate($data = array(), $tasks  = array() ) {
        DB::transaction(function()  use ($data, $tasks) {
            $roleId = DB::table($this->table)->insertGetId($data, 'id');

            $dataRoleTask = array();

            foreach ($tasks as $task) {
                $row = [
                    'role_id' => $roleId,
                    'task_id' => $task,
                ];
                array_push($dataRoleTask, $row);
            }

            DB::table('role_task')->insert($dataRoleTask);
        });
    }

    public function roleEdit($roleId, $data = array(), $tasks  = array() ) {
        DB::transaction(function()  use ($roleId, $data, $tasks) {
            Role::where('id', $roleId)->update($data);

            DB::table('role_task')->where('role_id', $roleId)->delete();

            $dataRoleTask = array();

            foreach ($tasks as $task) {
                $row = [
                    'role_id' => $roleId,
                    'task_id' => $task,
                ];
                array_push($dataRoleTask, $row);
            }

            DB::table('role_task')->insert($dataRoleTask);
        });
    }

}
