<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class Team extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'team';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_name', 'team_leader', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function teamsAll($companyId) {
        return DB::table($this->table)
            ->select('team.id', 'team.team_name', 'users.fname as leader_fname', 'users.lname as leader_lname', 'team_users.member_type')
            ->leftjoin('team_users', 'team.id', '=', 'team_users.team_id')
            ->join('users', 'team_users.user_id', '=' , 'users.id')
            ->where('team_users.member_type', 'leader')
            ->where('users.company_id', $companyId)
            ->orderby('team_name', 'ASC')
            ->get();
    }
}
