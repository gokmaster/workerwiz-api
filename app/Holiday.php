<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class Holiday extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'holiday';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'holiday_name', 'holiday_date', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function fetchCurrentyearAndNextYear($companyId) {
        $qry = "SELECT * FROM holiday
                WHERE 
                    (YEAR(holiday_date) = YEAR(CURDATE()) 
                OR 
                    YEAR(holiday_date) = YEAR(CURDATE()) + 1)
                AND company_id = $companyId
                ORDER BY holiday_date"; 

        return DB::select( DB::raw($qry) );
    } 
}
