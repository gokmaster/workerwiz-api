<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class Customfield extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'customfield';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'field_label','field_info','inputname','input_codename','input_type','form','form_section','form_section_sort','field_sort'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function fieldsOfFormFetch($formName, $campaignId, $companyId) {
        return $qry = DB::table($this->table)
                    ->select('customfield.id','customfield.field_label','customfield.field_info','customfield.inputname','customfield.input_codename','customfield.input_type',
                        'company_customfield.form_section','company_customfield.form_section_sort','company_customfield.field_sort','company_customfield.required')
                    ->where('customfield.form', $formName)
                    ->where(function ($qry) use ($campaignId) {
                        $qry->where('company_customfield.campaign_id', $campaignId)->orWhere('company_customfield.campaign_id', 0);
                    })
                    ->join('company_customfield', function($join) use ($companyId,$campaignId) {
                        $join->on('customfield.input_codename', '=', 'company_customfield.input_codename');
                        $join->where('company_customfield.company_id', $companyId);
                    })
                    ->orderBy('company_customfield.form_section_sort', 'ASC')
                    ->orderBy('company_customfield.field_sort', 'ASC')
                    ->get();
    }
}
