<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Confirmer;

class ConfirmerController extends Controller
{   
    public function all(Request $request) { 
        $data = $request->all();
        $model = new Confirmer;
        return $model->confirmerAll($data['company_id']);
    }

    public function userIsApprover(Request $request) {
        $userId = $request->input('user_id');
        $model = new Confirmer;
        return $model->userIsApprover($userId);
    }

    public function confirmersWithUsers(Request $request) {
        $data = $request->all();
        $model = new Confirmer;
        return $model->confirmersWithUsers($data['company_id']);
    }

    public function confirmerAdd(Request $request) {
        try {
            $companyId = $request->input('company_id');
            $selectedMembers = json_decode($request->input('selected_members'), true);

            if (count($selectedMembers)) {
                foreach($selectedMembers as $i=>$member) {
                    $selectedMembers[$i]['company_id'] = $companyId;
                }
            }

            $model = new Confirmer;
            $model->confirmersAdd($selectedMembers,$companyId);
            $response = [
                'success' => true,
                'message' => 'Successfully added confirmers',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }
}