<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Department;
use App\User;

class DepartmentController extends Controller
{   
    public function create(Request $request) {
        try {
            $data = $request->all();

            Department::create($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully added department',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function edit(Request $request)
    {
        try {
            $data = $request->except('id');
            $id = $request->input('id');

            Department::find($id)->update($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully edited department',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function delete(Request $request)
    {
        try {
            $id = $request->input('id');

            if (User::where('department_id', $id)->exists()) {
                throw new \Exception( "Cannot delete department because it is currently assigned to a user." ); 
            }

            Department::find($id)->delete();
                
            $response = [
                'success' => true,
                'message' => 'Successfully deleted department',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function all() {
        return Department::orderBy('department_name', 'ASC')->get();
    }
}