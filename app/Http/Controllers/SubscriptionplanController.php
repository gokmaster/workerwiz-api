<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Subscriptionplan;
use App\CompanySubscription;

class SubscriptionplanController extends Controller
{
    public function fetch(Request $request) {
        $data = $request->all();
        return Subscriptionplan::where('subscription_plan_no', $data['subscription_plan_no'])->first();
    }

    public function companySubscriptionCreate(Request $request) {
        try {
            $data = $request->all();
           
            $model = new CompanySubscription;
            $model->companySubscriptionCreate($data['subscription_plan_order_id']);

            $response = [
                'success' => true,
                'message' => 'Successfully created company subscription',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        } 
    }

    public function assignUnusedSubscriptionPlanToCompany(Request $request) {
        try {
            $data = $request->all();
           
            $model = new CompanySubscription;
            $assigned = $model->assignUnusedSubscriptionPlanToCompany($data['company_id']);

            if ($assigned == true) {
                $response = [
                    'success' => true,
                    'message' => 'Successfully assigned unused subscription-plan to company',
                ];
            } else {
                $response = [
                    'success' => false,
                    'message' => 'No unused subscription-plan to assign to company',
                ];
            }
             
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        } 
    }
}