<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Appwhitelist;

class AppWhitelistController extends Controller
{
    public function fetchAppId(Request $request) {
        $url = $request->input('url');
        return Appwhitelist::select('id','name')->where('url', $url)->first();
    }
}