<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\AttachmentType;
use App\ProfileAttachment;
use App\User;

class AttachmentController extends Controller
{
    public function attachmentTypeOfGroupFetch(Request $request) {
        $group = $request->input('group');
        return AttachmentType::select('title','codename')->where('group', $group)->orderBy('title')->get();
    }

    public function  profileAttachmentAdd(Request $request) {
        try {
            $data = $request->all();

            ProfileAttachment::create($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully added attachment',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function profileAttachmentEdit(Request $request) {
        try {
            $data = $request->except('id');
            $id = $request->input('id');

            ProfileAttachment::find($id)->update($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully edited attachment',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function profileAttachmentDelete(Request $request) {
        try {
            $id = $request->input('id');

            ProfileAttachment::find($id)->update(['deleted'=>1]);
                
            $response = [
                'success' => true,
                'message' => 'Successfully deleted attachment',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function profileAttachmentOfUser(Request $request) {
        $data = $request->all();

        $model = new ProfileAttachment;
        return $model->profileAttachmentOfUser($data['user_id']);
    }
}