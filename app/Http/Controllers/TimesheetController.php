<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Timesheet;

class TimesheetController extends Controller
{
    public function add(Request $request)
    {
        try {
            $data = $request->all();

            Timesheet::create($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully added entry into timesheet',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function edit(Request $request)
    {
        try {
            $data = $request->except('id');
            $id = $request->input('id');

            Timesheet::find($id)->update($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully edited timesheet entry',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function approveAll(Request $request)
    {
        try {
            $ids = json_decode($request->input('timesheet-ids'));

            Timesheet::whereIn('id', $ids)->update(['approved' => 1]);
                
            $response = [
                'success' => true,
                'message' => 'Successfully approved timesheet entries',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function currentWeekRecords(Request $request) {
        $data = $request->all();
        $model = new Timesheet;
        return $model->currentWeekRecords($data['user_id'], $data['timezone_utc_offset']);
    }

    public function lastWeekRecords(Request $request) {
        $data = $request->all();
        $model = new Timesheet;
        return $model->lastWeekTilYesterdayRecords($data['user_id'], $data['timezone_utc_offset']);
    }

    public function allUsersRecords(Request $request) {
        $data = $request->all();
        $model = new Timesheet;
        return $model->lastWeekRecordsForAllUsers($data['company_id'], $data['timezone_utc_offset']);
    }
}