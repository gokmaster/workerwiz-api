<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Payrollexcelupload;
use App\Payroll;
use App\Payslip;
use App\User;

class PayrollController extends Controller
{
    public function excelUploadAdd(Request $request) {
        try {
            $data = $request->all();

            $id = Payrollexcelupload::create($data)->id;
                
            $response = [
                'success' => true,
                'message' => 'Successfully added excel-upload record',
                'payroll_excelupload_id' => $id,
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function excelUploadEdit(Request $request) {
        try {
            $id = $request->input('id');
            $data = $request->except('id');

            Payrollexcelupload::find($id)->update($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully edited excel-upload record',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function uploadedExcelsFetch(Request $request) {
        $data = $request->all();
        return Payrollexcelupload::where('company_id',$data['company_id'])->orderBy('created_at', 'DESC')->take(10)->get();
    }

    public function payrollRecordsFetch(Request $request) {
        $data = $request->all();
        $model = new Payroll;
        return $model->fetchRecordsForUploadedExcel($data['payroll_excel_upload_id'], $data['payperiod_end'], $data['company_id']);
    }

    public function payrollRecordsAdd(Request $request) {
        try {
            $employees = json_decode($request->input('employees'), true);
            $companyId = $request->input('company_id');

            foreach ($employees as $emp) {
                $empId = $emp['emp_id'];
                if (!User::where('emp_id', $empId)->where('company_id', $companyId)->exists()) {
                    Payrollexcelupload::where('id', $emp['payroll_excel_upload_id'])->delete();
                    throw new \Exception("There is no user in your company with the following employee-ID: $empId"); 
                }
            }

            foreach ($employees as $employee) {
                if (isset($employee['customfields'])) {
                    $model = new Payroll;
                    $model->payrollWithCustomFieldsCreate($employee);
                } else {
                    Payroll::create($employee);
                }
            }
                           
            $response = [
                'success' => true,
                'message' => 'Successfully added payroll records',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function payslipAdd(Request $request) {
        try {
            $data = $request->all();

            Payslip::insert($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully added payslip records',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function payslipOfUser(Request $request) {
        $userId = $request->input('user_id');

        return Payslip::where('user_id', $userId)
                ->orderBy('payperiod_end', 'DESC')
                ->orderBy('created_at', 'DESC')
                ->take(52)->get();
    }
}