<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Holiday;

class HolidayController extends Controller
{
    public function create(Request $request) {
        try {
            $data = $request->all();

            Holiday::create($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully added holiday',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function edit(Request $request)
    {
        try {
            $data = $request->except('id');
            $id = $request->input('id');

            Holiday::find($id)->update($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully edited holiday',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function delete(Request $request)
    {
        try {
            $id = $request->input('id');

            Holiday::find($id)->delete();
                
            $response = [
                'success' => true,
                'message' => 'Successfully deleted holiday',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function fetch(Request $request) {
        $data = $request->all();
        $model = new Holiday;
        return $model->fetchCurrentyearAndNextYear($data['company_id']);
    }
}