<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Lcobucci\JWT\Parser;
use Carbon\Carbon;
use App\Oauthclient;
use App\User;
use App\Loginlog;
use App\Timesheet;
use App\Unverifieduser;
use App\UserInvite;

class UserController extends Controller
{   
    public function userInviteCreate(Request $request) {
        try {
            $data = $request->all();
            UserInvite::create($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully created user-invite',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function invitedUserCreate(Request $request) {
        try {
            $data = $request->all();

            if (!UserInvite::where('verification_code', $data['verification_code'])->exists()) {
                throw new \Exception("User verification failed"); 
            }

            $model = new User;
            $model->invitedUserCreate($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully created user account',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function invitedUserVerify(Request $request) {
        try {
            $data = $request->all();

            if (!UserInvite::where('verification_code', $data['verification_code'])->exists()) {
                throw new \Exception( "Email verification failed" ); 
            }

            if (UserInvite::where('account_created', 1)->exists()) {
                throw new \Exception( "Account already created." ); 
            }

            $model = new UserInvite;
            $user = $model->companyFetch($data['verification_code']);
                
            $response = [
                'success' => true,
                'message' => 'Successfully verified user invitation',
                'user' => $user,
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function unverifiedUserCreate(Request $request) {
        try {
            $data = $request->all();
            Unverifieduser::create($data);
                
            $response = [
                'success' => true,
                'message' => 'User registration successful',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function unverifiedUserEmailVerify(Request $request) {
        try {
            $data = $request->all();
            
            if (!Unverifieduser::where('verification_code', $data['verification_code'])->exists()) {
                throw new \Exception( "Email verification failed." ); 
            }

            if (Unverifieduser::where('verification_code', $data['verification_code'])->where('verified', 1)->exists()) {
                throw new \Exception( "Your email address has already been verified" ); 
            }

            $model = new User;
            $model->userAndCompanyCreate($data['verification_code']);

            $response = [
                'success' => true,
                'message' => 'Email verified successfully',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function create(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email|unique:users'
            ]);

            $data = $request->all();
            $model = new User;
            $model->userCreate($data);
                
            $response = [
                'success' => true,
                'message' => 'User registration successful',
            ]; 
            return response()->json($response); 
        } catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            $response = [
                'success' => false,
                'message' => $e->errors(),
            ];
            return response()->json($response);
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function edit(Request $request)
    {
        try {
            $data = $request->except('id');
            $id = $request->input('id');

            User::find($id)->update($data);
                
            $response = [
                'success' => true,
                'message' => 'User details edited successfully',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function delete(Request $request)
    {
        try {
            $id = $request->input('id');

            $data = [
                'status' => 'inactive'
            ];

            User::find($id)->update($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully deleted user',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function unarchive(Request $request)
    {
        try {
            $id = $request->input('id');

            $data = [
                'status' => 'active'
            ];

            User::find($id)->update($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully unarchived user',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function passwordChange(Request $request)
    {
        try {
            $pword = $request->input('password');
            $newPword = password_hash($request->input('new_password'), PASSWORD_BCRYPT);
            $userId = $request->input('user_id');

            $d = User::select('password')->where('id', $userId)->first();
            // if current password is incorrect
            if(!password_verify($pword, $d['password'])) {
                throw new \Exception( "Your current password is incorrect." ); 
            } 

            User::find($userId)->update(['password' => $newPword]);
                
            $response = [
                'success' => true,
                'message' => 'Password changed successfully',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function login(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            $email =  $request->input('email');
            $password = $request->input('password');

            if (!User::where('email', $email)->exists()) {
                throw new \Exception( "The email address, $email does not exist." ); 
            }

            $d = User::select('password')->where('email', $email)->first();
            // if password is incorrect
            if(!password_verify($password, $d->password)) {
                throw new \Exception( "Invalid password." ); 
            } 
            
            // get auth token if authenticated is successful
            $authResponse = json_decode($this->getAuthToken($email, $password), true);
            
            $userDetails = User::select('id', 'fname', 'lname','email')->where('email', $email)->first();
           
            $inputs = [
                'user_id' => $userDetails->id,
                'user_email' => $email,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'access_token' => $authResponse['access_token']
            ];
            $loginLogId = Loginlog::create($inputs)->id; // Log when user logins

            $model = new Timesheet;
            $model->checkinTimeInsert($userDetails->id); // insert checkin-time into timesheet
            
            $response = [
                'success' => true,
                'message' => 'Login successful',
                'user_details' => $userDetails,
                'loginlog_id' => $loginLogId,
                'auth' => $authResponse,
            ];    
            return response()->json($response);

        } catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            $response = [
                'success' => false,
                'message' => json_encode($e->errors()),
            ];
            return response()->json($response);

        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function logout(Request $request) {
        $token = $request->bearerToken();
               
        if ($token !== null) {
            $id = (new Parser())->parse($token)->getHeader('jti');
            DB::table('oauth_access_tokens')->where('id', '=', $id)->update(['revoked' => 1]);

            Loginlog::where('access_token', $token)
                ->update(['logout_time' => Carbon::now()]);

            $d = Loginlog::select('user_id')->where('access_token', $token)->first();

            $model = new Timesheet;
            $model->checkoutTimeUpdate($d->user_id);

            return [
                'success' => true,
                'message' => 'Logout successfully.'
            ];
        }
     
        return [
            'success' => true,
            'message' => 'Already Logged-out.'
        ];
    }

    public function getUserDetails(Request $request) {
        try {
            $data = $request->all();

            if (!User::where('id', $data['user_id'])->where('company_id', $data['company_id'])->exists()) {
                throw new \Exception("User does not exist in your company"); 
            }
            
            $model = new User;
            $user = json_decode(json_encode($model->userFetch($data['user_id'], $data['company_id'])), true);
            return $user[0];
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function userdetails() {
        // needed for verifying authenticated user
        return $user = app('auth')->guard()->user();
    }

    public function allArchivedUsers(Request $request) {
        $data = $request->all();
        return User::select('id', 'emp_id', 'fname', 'lname', 'email', 'dob')
                    ->where('status', 'inactive')
                    ->where('company_id', $data['company_id'])
                    ->get();
    }

    public function allUsers(Request $request) {
        $data = $request->all();
        $model = new User;
        return $model->userAll($data['company_id']);
    }

    public function allUsersIds(Request $request) {
        $data = $request->all();
        return User::select('id', 'emp_id', 'fname', 'lname')
                ->where('status', 'active')
                ->where('company_id', $data['company_id'])
                ->orderBy('fname', 'ASC')->get();
    }

    public function  getAccessTokenUsingLoginLogId(Request $request) {
        $secretKey = $request->input('secretkey');

        if ($secretKey == "qaz890pl") {
            $loginLogId = $request->input('loginlog_id');
            return Loginlog::select('access_token')->where('id', $loginLogId)->first();
        }
    }
    
    // Returns auth token if authentication is successful
    private function getAuthToken($email, $password) {
        $client = Oauthclient::select('id', 'secret')
                    ->where(DB::raw("TRIM(name)"), "Password Grant Client")
                    ->first();

        $params = [
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => $email,
            'password' => $password,
            'scope' => '*',
        ];

        // call the '/oauth/token' route internally within this app to get auth token
        $req = Request::create('/oauth/token', 'POST', $params);
        $res = app()->handle($req);
        return $res->getContent();
    }
}