<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Customer;
use App\CustomfieldValue;

class CustomerController extends Controller
{
    public function create(Request $request) {
        try {
            $data = $request->all();
            $fname = strtolower(trim($request->input('fname')));
            $lname = strtolower(trim($request->input('lname')));
            $email = strtolower(trim($request->input('email')));
            $phone = trim($request->input('phone'));
            $companyId = $request->input('company_id');
            $campaignId = $request->input('campaign_id');

            if (Customer::whereRaw('( company_id = (?) AND lower(fname) = (?) AND lower(lname) = (?) AND campaign_id = (?)) 
                            AND (lower(email) = (?) OR phone = (?) )', [$companyId,$fname,$lname,$campaignId,$email,$phone] )->exists()) {
                throw new \Exception("Customer already exists"); 
            }
            
            $customFieldData = $request->except('fname','lname','phone','email','address1','address2','city','state','zipcode','country',
                'postal_address1','postal_address2','postal_city','postal_state','postal_zipcode','postal_country','preference','price_comfort',
                'creditcard_number','cardholder_name','card_expiry_date','cvv','campaign_id','company_id');

            if (!empty($customFieldData)) { // if there are custom fields
                $customerData = array_diff($data,$customFieldData);
            
                $model = new Customer;
                $model->customerWithCustomFieldsCreate($customerData,$customFieldData);
            } else {
                Customer::create($data);
            }
           
            $response = [
                'success' => true,
                'message' => 'Successfully created customer',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function edit(Request $request) {
        try {
            $data = $request->except('customer_id');
            $id = $request->input('customer_id');

            if (!Customer::where('id',$id)->where('company_id',$data['company_id'])->exists()) {
                throw new \Exception("This customer does not exist in your company"); 
            }
            
            $customFieldData = $request->except('customer_id','fname','lname','phone','email','address1','address2','city','state','zipcode','country',
                'postal_address1','postal_address2','postal_city','postal_state','postal_zipcode','postal_country','preference','price_comfort',
                'creditcard_number','cardholder_name','card_expiry_date','cvv','campaign_id','company_id');

            if (!empty($customFieldData)) { // if there are custom fields
                $customerData = array_diff($data,$customFieldData);
            
                $model = new Customer;
                $model->customerWithCustomFieldsEdit($customerData,$customFieldData,$id);
            } else {
                Customer::find($id)->update($data);
            }
           
            $response = [
                'success' => true,
                'message' => 'Successfully edited customer',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function fetchByNameEmailPhone(Request $request) {
        $fname = strtolower(trim($request->input('fname')));
        $lname = strtolower(trim($request->input('lname')));
        $email = strtolower(trim($request->input('email')));
        $phone = trim($request->input('phone'));
        $companyId = $request->input('company_id');

        return Customer::whereRaw('( company_id = (?) AND lower(fname) = (?) AND lower(lname) = (?) ) 
                                    AND (lower(email) = (?) OR phone = (?) )',
                                    [$companyId,$fname,$lname,$email,$phone] )->first();
    }

    public function fetchAllCustomersOfCompany(Request $request) {
        $data = $request->all();

        return Customer::select('customer.*','campaign.campaign_name')
                        ->where('customer.company_id', $data['company_id'])
                        ->where('status', 'active')
                        ->leftjoin('campaign', 'customer.campaign_id','=','campaign.id')
                        ->get();
    }

    public function customerDetails(Request $request) {
        try {
            $data = $request->all();

            if (!Customer::where('id', $data['customer_id'])->where('company_id', $data['company_id'])->exists()) {
                throw new \Exception("Customer does not exist in your company"); 
            }

            if(CustomfieldValue::where('linkedtable','customer')->where('linkedtable_id', $data['customer_id'])->exists()) {
                $model = new Customer;
                return $model->customerWithCustomFieldsFetch($data['customer_id'], $data['company_id']);
            } else {
                return Customer::where('id', $data['customer_id'])->first();
            }
            
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }
}