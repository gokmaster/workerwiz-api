<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Campaign;

class CampaignController extends Controller
{
    public function campaignsOfCompanyFetch(Request $request) {
        $companyId = $request->input('company_id');
        return Campaign::where('company_id', $companyId)->orderBy('campaign_name', 'ASC')->get();
    }
}