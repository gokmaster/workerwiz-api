<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Mailinglist;
use App\Mailinggroup;

class MailinglistController extends Controller {
   public function create(Request $request) {        
      try {
         $data = $request->all();

         $model = new Mailinglist;
         $model->mailinglistAndMailingGroupCreate($data);
             
         $response = [
            'success' => true,
            'message' => 'Successfully created mailing list',
         ]; 
         return response()->json($response); 
      } catch(\Exception $e) {
         // When query fails. 
         $response = [
            'success' => false,
            'message' => $e->getMessage(),
         ];
         return response()->json($response);
      }
   }

   public function fetchAllMailingGroupsOfCompany(Request $request) {        
      $data = $request->all();
      return Mailinggroup::where('company_id', $data['company_id'])
               ->orderBy('title', 'ASC')->get();
   }

   public function fetchAllMailingListOfMailingGroup(Request $request) {        
      $data = $request->all();
      return Mailinglist::select('fname','lname','email')
               ->where('mailinggroup_id', $data['mailinggroup_id'])
               ->where('company_id', $data['company_id'])
               ->get();
   }

}