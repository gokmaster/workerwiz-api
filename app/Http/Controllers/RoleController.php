<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Task;
use App\Role;
use App\Roletask;

class RoleController extends Controller
{
    public function tasksAll(){
        $tasks = Task::orderBy('label', 'asc')
                    ->orderBy('task_name', 'asc')
                    ->where('is_task',1)->get();

        $tasks_array = array();
        $task_label = "";

        $a = 0;
        $b = 0;

        // group tasks by label
        foreach($tasks as $task) {
            // if this is the first loop
            if ($a == 0 && $b == 0) {
                $task_label = $task->label;
            }

            // Since tasks is already sorted by label when fetched from DB,
            // we know it is a different label group when label is different from the one stored in $task_label
            if ( $task->label != $task_label ) {
                $task_label = $task->label;
                $a++;
                $b = 0;
            }

            $tasks_array[$a][$b] = [
                'task_id' => $task->id,
                'task_name' => $task->task_name,
                'label' => $task->label,
                'privilege_level' => $task->privilege_level,
            ];

            $b++;
        }
        return $tasks_array;
    }

    public function create(Request $request) {
        try {
            $this->validate($request, [
                'role_name' => 'required'
            ]);
            $postdata = $request->all();

            $data = [
                'role_name' => $postdata['role_name'],
                'company_id' => $postdata['company_id']
            ];

            $model = new Role;
            $model->roleCreate($data, $postdata['tasks']);
                
            $response = [
                'success' => true,
                'message' => 'Department created successful',
            ]; 
            return response()->json($response); 
        } catch (\Illuminate\Validation\ValidationException $e ) {
            // When there is any invalid input
            $response = [
                'success' => false,
                'message' => $e->errors(),
            ];
            return response()->json($response);
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function edit(Request $request) {
        try {
            $postdata = $request->all();

            $data = [
                'role_name' => $postdata['role_name'],
            ];

            if (!Role::where('id', $postdata['role_id'])->where('company_id', $postdata['company_id'])->exists()) {
                throw new \Exception("The department you trying to edit does not exist in your company"); 
            }

            $model = new Role;
            $model->roleEdit($postdata['role_id'], $data, $postdata['tasks']);
                
            $response = [
                'success' => true,
                'message' => 'Department edited successful',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function all(Request $request) {
        $data = $request->all();
        return Role::where('company_id',$data['company_id'])->orderBy('role_name', 'ASC')->get();
    }

    public function roleMenu(Request $request) {
        // This will return the menu items for the sidebar menu

        $user_id =  $request->input('user_id');
        $model = new Task;
        return $model->roleMenu($user_id);
    }

    public function roleFetch(Request $request) {
        try {
            $roleId = $request->input('role_id');
            $companyId = $request->input('company_id');

            if (!Role::where('id', $roleId)->where('company_id', $companyId)->exists()) {
                throw new \Exception("The department you trying to access does not exist in your company"); 
            }

            $role = Role::select('role_name')->where('id', $roleId)->first();
            $rt = Roletask::select('task_id')->where('role_id', $roleId)->get();

            $roleTasks = [];

            foreach ($rt as $task) {
                array_push($roleTasks, $task['task_id']);
            }

            return [
                'role' => $role,
                'roletasks' => $roleTasks,
            ];
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function allowedTasksOfUser(Request $request) {
        $userId =  $request->input('user_id');

        $model = new Task;
        $allTasks  = $model->fetchAll();

        $rModel = new Roletask;
        $tasks = $rModel->userRoleTasks($userId);
        $tasks = json_decode(json_encode($tasks), true); // convert object to array

        $userTasks = array_column($tasks, 'task_id');

        $response = [
            'all_tasks' => $allTasks,
            'user_task_ids' => $userTasks,
        ];    

        return response()->json($response);
    }

    public function userPermissions(Request $request) {
        $userId =  $request->input('user_id');
        $model = new Roletask;
        return $model->userRoleTasks($userId);
    }

    public function taskFetch(Request $request) {
        $frontendView = $request->input('frontend_view');
        return Task::where('frontend_view', $frontendView)->first();
    }
}