<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\MailConfig;

class MailConfigController extends Controller {
   public function create(Request $request) {        
      try {
         $data = $request->all();

         MailConfig::create($data);
             
         $response = [
             'success' => true,
             'message' => 'Successfully added mail server settings',
         ]; 
         return response()->json($response); 
      } catch(\Exception $e) {
         // When query fails. 
         $response = [
             'success' => false,
             'message' => $e->getMessage(),
         ];
         return response()->json($response);
      }
   }

   public function edit(Request $request) {        
      try {
         $data = $request->except('id');
         $id = $request->input('id');

         MailConfig::find($id)->update($data);
             
         $response = [
             'success' => true,
             'message' => 'Successfully edited mail server settings',
         ]; 
         return response()->json($response); 
      } catch(\Exception $e) {
         // When query fails. 
         $response = [
             'success' => false,
             'message' => $e->getMessage(),
         ];
         return response()->json($response);
      }
   }

   public function mailServerChange(Request $request) {
      try {
         $id = $request->input('mailconfig_id');

         $model = new MailConfig;
         $model->changeMailServer($id);

         $response = [
            'success' => true,
            'message' => 'Successfully changed mail server',
        ]; 
        return response()->json($response); 

      } catch(\Exception $e) {
         // When query fails. 
         $response = [
             'success' => false,
             'message' => $e->getMessage(),
         ];
         return response()->json($response);
      }
   }

   public function fetch(Request $request) {
      $data = $request->all();

      if ($data['secretkey'] != env("API_CALL_KEY")) {
         return "Permission denied";
      }

      if ($request->has('in_use')) {
         return MailConfig::where('in_use', 1)->first();
      }

      return MailConfig::where('codename',$data['codename'])->first();
   }

   public function fetchAll(Request $request) {
      $data = $request->all();
      return MailConfig::where('company_id',$data['company_id'])->get();
   }
}