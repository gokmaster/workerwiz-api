<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\SubscriptionPlanOrder;
use App\User;

class SubscriptionPlanOrderController extends Controller
{
    public function create(Request $request) {
        try {
            $data = $request->all();

            $countActiveUsers = User::where('status', 'active')->where('company_id',$data['company_id'])->count();

            if ($countActiveUsers > $data['user_quantity']) {
                throw new \Exception( "The Number of Users should NOT be less than the number of active user accounts you currently have." );
            }

            $id = SubscriptionPlanOrder::create($data)->id;
                
            $response = [
                'success' => true,
                'message' => 'Successfully created order',
                'subscription_plan_order_id' => $id,
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }       
    }

    public function edit(Request $request) {
        try {
            $data = $request->except('subscription_plan_order_id');
            $id = $request->input('subscription_plan_order_id');

            if (!SubscriptionPlanOrder::where('id',$id)->where('company_id',$data['company_id'])->where('paid',0)->exists()) {
                throw new \Exception( "Invalid subscription-plan-order-ID." ); 
            }

            SubscriptionPlanOrder::where('id',$id)->update($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully edited subscription-plan order',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }       
    }

    public function allUnpaidFetch() {
        $model = new SubscriptionPlanOrder;
        return $model->allUnpaidFetch();
    }

    public function userWhoCreatedOrder(Request $request) {
        $data = $request->all();
        $model = new SubscriptionPlanOrder;
        return $model->userWhoCreatedOrder($data['subscription_plan_order_id']);
    }
}