<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Team;
use App\Teamusers;

class TeamController extends Controller
{
    public function create(Request $request)
    {
        try {
            $data = $request->all();

            $data2 = $data;
            unset($data2['team_leader']); // remove team_leader from array

            $teamId = Team::create($data2)->id;

            Teamusers::create([
                'team_id' => $teamId,
                'user_id' => $data['team_leader'],
                'member_type' =>  $data['member_type']
            ]);
                
            $response = [
                'success' => true,
                'message' => 'Successfully created team',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function all(Request $request) {
        $data = $request->all();
        $model = new Team;
        return $model->teamsAll($data['company_id']);
    }

    public function teamUsers(Request $request) {
        try {
            // returns all users of all teams
            $data = $request->all();

            if (!Team::where('id', $data['team_id'])->where('company_id', $data['company_id'])->exists()) {
                throw new \Exception("The team you trying to access does not exist in your company"); 
            }

            $model = new Teamusers;
            return $model->teamUsers($data['team_id'], $data['company_id']);
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function teamMembers(Request $request) {
        // returns users of a team
        $data = $request->all();

        $model = new Teamusers;
        return $model->teamMembers($data['team_id'],$data['company_id']);
    }

    public function membersEdit(Request $request) {
        try {
            $selected_members = json_decode($request->input('selected_members'), true);

            $teamId = $selected_members[0]['team_id'];

            $model = new Teamusers;
            $model->teamMembersEdit($selected_members, $teamId);
            $response = [
                'success' => true,
                'message' => 'Successfully edited members',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function teamMembersOfUser(Request $request) {
        $data = $request->all();
        
        $model = new Teamusers;
        return $model->teamMembersOfUser($data['user_id'], $data['company_id']);
    }

}