<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Rep;

class RepController extends Controller
{   
    public function create(Request $request) {
        try {
            $data = $request->all();

            Rep::create($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully added rep',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function edit(Request $request)
    {
        try {
            $data = $request->except('id');
            $id = $request->input('id');

            Rep::where('id',$id)->where('company_id',$data['company_id'])->update($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully edited rep',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function delete(Request $request)
    {
        try {
            $data = $request->all();

            Rep::where('id',$data['id'])->where('company_id',$data['company_id'])->update(['status' => 'inactive']);
                
            $response = [
                'success' => true,
                'message' => 'Successfully deleted rep',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function all(Request $request) {
        $data = $request->all();
        return Rep::where('status', 'active')->where('company_id',$data['company_id'])
                    ->orderBy('fname', 'ASC')->get();
    }

    public function fetchRepByIdAndProjectManager(Request $request) {
        $repId = $request->input('id');
        $companyId = $request->input('company_id');
        return Rep::where([['id', $repId],['company_id',$companyId]])
                    ->orwhere([['type','project-manager'],['status','active'],['company_id',$companyId]])
                    ->get();
    }

    public function fetchProjectManager(Request $request) {
        $companyId = $request->input('company_id');
        return Rep::where([['type','project-manager'],['status','active'],['company_id',$companyId]])->first();
    }
}