<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Customfield;
use App\CustomfieldValue;

class CustomfieldController extends Controller
{
    public function fieldsOfFormFetch(Request $request) {
        $data = $request->all();

        $model = new Customfield;
        return $model = $model->fieldsOfFormFetch($data['form'], $data['campaign_id'], $data['company_id']);
    }

    public function fieldValuesOfLinkedTableRowFetch(Request $request) {
        $data = $request->all();
        return CustomfieldValue::select('input_codename','input_value')->where('linkedtable', $data['linkedtable'])->where('linkedtable_id',$data['linkedtable_id'])->get();
    }
}