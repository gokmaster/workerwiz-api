<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\PasswordReset;
use App\User;

class PasswordController extends Controller
{
    public function passwordResetRequestCreate(Request $request) {
        try {
            $data = $request->all();
            $email = $data['email'];

            if (!User::where('email', $email)->exists()) {
                throw new \Exception( "The email address, $email does not exist." ); 
            }

            $user = User::select('password')->where('email',$email)->first()->makeVisible(['password']);
            $user = json_decode(json_encode($user),true); // convert object to array

            $data['old_password'] = $user['password'];
            PasswordReset::create($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully submitted password-reset request',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
       
    }

    public function passwordResetConfirm(Request $request) {
        try {
            $data = $request->all();
            
            if (!PasswordReset::where('verification_code', $data['verification_code'])->exists()) {
                throw new \Exception("Email verification failed."); 
            }

            if (PasswordReset::where('verification_code', $data['verification_code'])->where('resetted', 1)->exists()) {
                throw new \Exception("You have already used this link to change your password"); 
            }

            $model = new PasswordReset;
            $model->passwordChangeAfterConfirmation($data['verification_code']);

            $response = [
                'success' => true,
                'message' => 'You have successfully changed your password',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }
}