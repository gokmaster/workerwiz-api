<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\EmailQue;

class EmailQueController extends Controller {
   public function create(Request $request) {        
      try {
         $data = $request->all();

         EmailQue::insert($data);

         $response = [
            'success' => true,
            'message' => 'Successfully added emails to que. Up to 50 emails will be sent out every minute',
         ]; 
         return response()->json($response); 
      } catch(\Exception $e) {
         // When query fails. 
         $response = [
            'success' => false,
            'message' => $e->getMessage(),
         ];
         return response()->json($response);
      }
   }

   public function markAsSent(Request $request) {    
      try {
         $data = $request->all();

         if ($data['secretkey'] != env("API_CALL_KEY")) {
            return "Permission denied";
         }

         EmailQue::where('id', $data['emailque_id'])->delete();

         $response = [
            'success' => true,
            'message' => 'Successfully marked email as sent',
         ]; 
         return response()->json($response); 
      } catch(\Exception $e) {
         // When query fails. 
         $response = [
            'success' => false,
            'message' => $e->getMessage(),
         ];
         return response()->json($response);
      }
   }

   public function fetchUnsentBatch(Request $request) {
      $secretkey = $request->input('secretkey');

      if ($secretkey != env("API_CALL_KEY")) {
         return "Permission denied";
      }

      $model = new EmailQue;
      return $model->fetchUnsentBatch();
   }
}