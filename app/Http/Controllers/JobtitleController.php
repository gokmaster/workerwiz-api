<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Jobtitle;
use App\User;

class JobtitleController extends Controller
{   
    public function create(Request $request) {
        try {
            $data = $request->all();

            Jobtitle::create($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully added job title',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function edit(Request $request)
    {
        try {
            $data = $request->except('id');
            $id = $request->input('id');

            Jobtitle::find($id)->update($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully edited job title',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function delete(Request $request)
    {
        try {
            $id = $request->input('id');

            if (User::where('job_title_id', $id)->exists()) {
                throw new \Exception( "Cannot delete job title because it is currently assigned to a user." ); 
            }

            Jobtitle::find($id)->delete();
                
            $response = [
                'success' => true,
                'message' => 'Successfully deleted job title',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function all(Request $request) {
        $data = $request->all();
        return Jobtitle::where('company_id', $data['company_id'])->orderBy('job_title', 'ASC')->get();
    }
}