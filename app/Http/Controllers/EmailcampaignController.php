<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\EmailCampaign;

class EmailcampaignController extends Controller {
   public function create(Request $request) {        
      try {
         $data = $request->all();

         EmailCampaign::create($data);
             
         $response = [
             'success' => true,
             'message' => 'Successfully created email campaign',
         ]; 
         return response()->json($response); 
      } catch(\Exception $e) {
         // When query fails. 
         $response = [
             'success' => false,
             'message' => $e->getMessage(),
         ];
         return response()->json($response);
      }
   }

   public function fetch(Request $request) {
      $data = $request->all();
      return EmailCampaign::where('id', $data['email_campaign_id'])->where('company_id', $data['company_id'])->first();
   }

   public function fetchAllOfCompany(Request $request) {
      $data = $request->all();
      return EmailCampaign::select('id','campaign_name','created_at')
               ->where('company_id', $data['company_id'])
               ->orderBy('created_at', 'DESC')->get();
   }
}