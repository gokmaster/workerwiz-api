<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Appointment;
use App\Confirmer;

class AppointmentController extends Controller
{   
    public function create(Request $request) {
        try {
            $data = $request->all();

            $whereCondition = [
                'rep_id'=> $data['rep_id'],
                'appointment_date' => $data['appointment_date'],
            ];
            
            if (!Appointment::where($whereCondition)->exists()) {
                $data['first_for_day'] = 1;
            }

            $model = new Appointment;
            $apptDate = $data['appointment_date'];
            $apptTime = $data['appointment_time'];
            $blockedCount = $model->timeslotBlocked($apptDate, $apptTime, $data['rep_id']);
            $blockedCount = json_decode(json_encode($blockedCount),true); // convert to array
            
            if ($blockedCount[0]['num'] > 0) { // if timeslot for rep has been blocked
                throw new \Exception( "Appointment cannot be scheduled to $apptDate $apptTime because this timeslot has been blocked"); 
            }

            Appointment::create($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully created appointment',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function edit(Request $request)
    {
        try {
            $data = $request->except('id');
            $id = $request->input('id');

            if (isset($data['appointment_date']) && isset($data['appointment_time'])) {
                $model = new Appointment;
                $apptDate = $data['appointment_date'];
                $apptTime = $data['appointment_time'];
                $blockedCount = $model->timeslotBlocked($apptDate, $apptTime, $data['rep_id']);
                $blockedCount = json_decode(json_encode($blockedCount),true); // convert to array
                
                if ($blockedCount[0]['num'] > 0) { // if timeslot for rep has been blocked
                    throw new \Exception( "Appointment cannot be scheduled to $apptDate $apptTime because this timeslot has been blocked"); 
                }
            }
          
            Appointment::where('id',$id)->where('company_id',$data['company_id'])->update($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully edited appointment',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function reschedule(Request $request) {
        try {
            $data = $request->all();

            $d = Appointment::select('reschedule_appointment_id')->where('id', $data['id'])->first();
            
            if ($d->reschedule_appointment_id != null) {
                throw new \Exception("Sorry, this appointment has already been rescheduled.");
            }

            $appt = Appointment::select('rep_id','business_name','client_type','phone','mobile',
                                        'street_address','city','zipcode','country','fname','lname',
                                        'bill_amount','email','company_id')
                                ->where('id', $data['id'])->where('company_id',$data['company_id'])->first();

            $appt = json_decode(json_encode($appt),true); // convert object to array
            $appt['appointment_date'] = $data['appointment_date'];
            $appt['appointment_time'] = $data['appointment_time'];
            $appt['status'] = $data['status'];
      
            $id = Appointment::create($appt)->id;

            Appointment::where('id', $data['id'])
                        ->where('company_id',$data['company_id'])
                        ->update(['reschedule_appointment_id'=>$id]);
                
            $response = [
                'success' => true,
                'message' => 'Successfully scheduled appointment',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function blockTimeslots(Request $request) {
        try {
            $data = $request->except('start_time','end_time');
            $starttime = $request->input('start_time');
            $endtime = $request->input('end_time');
           
            $startTimeInt = (int) date('H', strtotime($starttime));
            $endTimeInt = (int) date('H', strtotime($endtime));

            for ($i=$startTimeInt; $i<$endTimeInt; $i++) {
                $time = $i.":00:00";
                $whereCondition = [
                    'rep_id' => $data['rep_id'],
                    'appointment_date' => $data['appointment_date'],
                    'appointment_time' => $time,
                ];
                if (!Appointment::where($whereCondition)->exists()) {
                    $data['appointment_time'] = $time;
                    Appointment::create($data);
                }
            }
                      
            $response = [
                'success' => true,
                'message' => 'Successfully blocked appointment timeslots',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function blockedTimeslotRemove(Request $request) {
        try {
            $data = $request->all();

            Appointment::where([['id',$data['id']],['status','blocked'],['company_id',$data['company_id']]])->delete();
                                 
            $response = [
                'success' => true,
                'message' => 'Successfully unblocked appointment timeslot',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function fetchIcedAppointments(Request $request) {
        $data = $request->all();
        $model = new Appointment;
        return $model->fetchIcedAppointments($data['company_id']);
    }

    public function fetchDroppedAppointments(Request $request) {
        $data = $request->all();
        $model = new Appointment;
        return $model->fetchDroppedAppointments($data['company_id']);
    }

    public function fetchArchivedAppointments(Request $request) {
        $data = $request->all();
        $model = new Appointment;
        return $model->fetchArchivedAppointments($data['company_id']);
    }

    public function fetch(Request $request) {
        $data = $request->all();
        $model = new Appointment;
        return response()->json($model->fetch($data['id'], $data['company_id']));
    }

    public function fetchByRep(Request $request) { // fetch appointments grouped by rep
        $data = $request->all();
        $model = new Appointment;
        return $model->fetchPast6MonthsAndNext6MonthsByRep($data['company_id']);
    }

    public function fetchForConfirmer(Request $request) {
        $data = $request->all();

        $confirmerModel = new Confirmer;
        $userIsApprover = $confirmerModel->userIsApprover($data['user_id']);

        $model = new Appointment;

        if ($userIsApprover == 0) {
            return $model->fetchLast2WeeksAndNext6MonthsForConfirmer($data['user_id'],$data['company_id']);
        } else {
            return $model->fetchLast2WeeksAndNext6MonthsToConfirm($data['user_id'],$data['company_id']);
        }
    }
   
}