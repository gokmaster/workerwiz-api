<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Sale;
use App\Customer;
use App\CustomfieldValue;

class SaleController extends Controller
{
    public function create(Request $request) {
        try {
            $data = $request->all();
            $fname = strtolower(trim($request->input('fname')));
            $lname = strtolower(trim($request->input('lname')));
            $email = strtolower(trim($request->input('email')));
            $phone = trim($request->input('phone'));
            $companyId = $request->input('company_id');

            $customFieldData = $request->except('fname','lname','phone','email','address1','address2','city','state','zipcode','country',
                'postal_address1','postal_address2','postal_city','postal_state','postal_zipcode','postal_country','preference','price_comfort',
                'creditcard_number','cardholder_name','card_expiry_date','cvv','company_id', 'campaign_id','description','sku','quantity',
                'saleprice','notes','sale_agent');

            if (!empty($customFieldData)) { // if there are custom fields
                $saleData = array_diff($data,$customFieldData);
            
                $model = new Sale;
                $model->saleWithCustomFieldsCreate($saleData,$customFieldData);
            } else {
                Sale::create($data);
            }

            $customerData = $request->except('campaign_id','description','sku','quantity','saleprice','notes','sale_agent');

            // create customer record if not already exist
            if (!Customer::whereRaw('( company_id = (?) AND lower(fname) = (?) AND lower(lname) = (?) ) 
                            AND (lower(email) = (?) OR phone = (?) )', [$companyId,$fname,$lname,$email,$phone] )->exists()) {
                Customer::create($customerData);
            } else { // update customer record
                Customer::whereRaw('( company_id = (?) AND lower(fname) = (?) AND lower(lname) = (?) ) 
                            AND (lower(email) = (?) OR phone = (?) )', [$companyId,$fname,$lname,$email,$phone] )->update($customerData);
            }
                
            $response = [
                'success' => true,
                'message' => 'Successfully added sale',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function edit(Request $request) { 
        try {
            $data = $request->except('sale_id');
            $id = $request->input('sale_id');

            if (!Sale::where('id',$id)->where('company_id',$data['company_id'])->exists()) {
                throw new \Exception("This sale does not exist in your company"); 
            }
          
            $customFieldData = $request->except('sale_id','fname','lname','phone','email','address1','address2','city','state','zipcode','country',
                'postal_address1','postal_address2','postal_city','postal_state','postal_zipcode','postal_country','preference','price_comfort',
                'creditcard_number','cardholder_name','card_expiry_date','cvv','company_id', 'campaign_id','description','sku','quantity',
                'saleprice','notes','sale_agent');

            if (!empty($customFieldData)) { // if there are custom fields
                $saleData = array_diff($data,$customFieldData);
            
                $model = new Sale;
                $model->saleWithCustomFieldsEdit($saleData,$customFieldData,$id);
            } else {
                Sale::find($id)->update($data);
            }

            $response = [
                'success' => true,
                'message' => 'Successfully edited sale',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function salesOfYear(Request $request) {
        $data = $request->all();
        $model = new Sale;
        return $model->salesOfYear($data['year'], $data['company_id']);
    }

    public function salesOfAgentForYear(Request $request) {
        $data = $request->all();
        $model = new Sale;
        return $model->salesOfAgentForYear($data['user_id'],$data['year'], $data['company_id']);
    }

    public function saleDetails(Request $request) {
        try {
            $data = $request->all();

            if (!Sale::where('id', $data['sale_id'])->where('company_id', $data['company_id'])->exists()) {
                throw new \Exception("This sale does not exist in your company"); 
            }

            $model = new Sale;
            if (CustomfieldValue::where('linkedtable','sale')->where('linkedtable_id', $data['sale_id'])->exists()) {       
                return $model->saleWithCustomFieldsFetch($data['sale_id'], $data['company_id']);
            } else {
                return $model->saleFetch($data['sale_id']);
            }
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }
}