<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Company;

class CompanyController extends Controller
{
    public function fetch(Request $request) {
        $companyId = $request->input('company_id');
        return Company::where('id', $companyId)->first();
       
    }
}