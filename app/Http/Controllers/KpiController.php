<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Kpigroup;
use App\Kpidetail;
use App\Kpigroupdetail;
use App\Kpiuser;

class KpiController extends Controller
{
    public function kpiGroupCreate(Request $request)
    {
        try {
            $data = $request->all();

            Kpigroup::create($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully created KPI group',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function kpiDetailCreate(Request $request)
    {
        try {
            $data = $request->all();

            Kpidetail::create($data);
                
            $response = [
                'success' => true,
                'message' => 'Successfully added KPI detail',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function kpiGroupAll(Request $request) {
        $data = $request->all();
        return Kpigroup::where('company_id',$data['company_id'])
                        ->orderBy('description', 'ASC')->get();
    }

    public function kpiDetailAll(Request $request) {
        $data = $request->all();
        return Kpidetail::where('company_id',$data['company_id'])
                        ->orderBy('description', 'ASC')->get();
    }

    public function kpiOfGroup(Request $request) {
        $kpiGrpId = $request->input('kpi_group_id');

        return Kpigroupdetail::select('kpi_detail_id')
                                ->where('kpi_group_id', $kpiGrpId)->get();
    }

    public function kpiAssignToGroup(Request $request) {
        try {
            $data = $request->all();

            $model = new Kpigroupdetail;
            $model->edit($data['kpi_group_id'], json_decode($data['kpi_detail_ids']) );
                            
            $response = [
                'success' => true,
                'message' => 'Successfully assigned KPI to Group',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function kpiAssignToUser(Request $request) {
        try {
            $data = $request->all();

            $model = new Kpiuser;
            $d =  $model->insert($data);

            if ($d['already_exist'] == true) {
                throw new \Exception( "User has already been assigned KPI for that period." ); 
            }
                            
            $response = [
                'success' => true,
                'message' => 'Successfully assigned KPI to user',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function userKpisForEntry(Request $request) {
        $userId = $request->input('user_id');

        $model = new Kpiuser;
        $data = json_decode(json_encode($model->lastWeekTilYesterdayRecords($userId)), true);

        $userKpis = [];
        $row = [];
        
        // Create 2D array where each unique kpi_description represents one row
        foreach ($data as $key=>$d) {
            if (empty($row)) {
                $row[]= $d; // 1st iteration so create 1st row
            } else {
                if ($row[0]['kpi_description'] == $d['kpi_description']) {
                    // kpi_description matches so they belong to the same row
                    $row[] = $d;
                } else {
                    // different kpi_description so push existing row into array
                    // then clear row to store next kpi_description
                    array_push($userKpis, $row);
                    unset($row); 
                    $row[] = $d; // new kpi_description so it should be in a different row from previous row
                }
            }
        }

        array_push($userKpis, $row); // push last row

        $dateHeader = array_column($row, 'kpi_date');
        return [
            'userKpis' => $userKpis,
            'dateHeader' => $dateHeader, 
        ];
    }

    public function userKpiUpdate(Request $request) {
        try {
            $data = json_decode($request->input('datajson'));

            $model = new Kpiuser;
            $d =  $model->userKpiUpdate($data);
                            
            $response = [
                'success' => true,
                'message' => 'Successfully updated user-KPIs',
            ]; 
            return response()->json($response); 
        } catch(\Exception $e) {
            // When query fails. 
            $response = [
                'success' => false,
                'message' => $e->getMessage(),
            ];
            return response()->json($response);
        }
    }

    public function kpiBetweenDates(Request $request) {
        $data = $request->all();
        
        $model = new Kpiuser;
        $kpis =  json_decode(json_encode($model->kpiBetweenDates($data)), true);

        $userKpis = [];
        $row = [];
            
        // Create array where each row 
        // represents a unique kpi_description for a user
        foreach ($kpis as $key=>$d) {
            if (empty($row)) {
                // 1st iteration so create 1st row
                $row['user_id'] = $d['user_id'];
                $row['user_name'] = $d['fname'] . " " . $d['lname'];
                $row['kpi_description'] = $d['kpi_description'];
                $row['result'] = $d['result'];
                $row['target'] = $d['target'];
                $row['target_type'] = $d['target_type'];
                $row['rep'] = [];

                 // if not empty string nor null
                if (!empty($d['rep'])) {
                    $d['rep'] = json_decode($d['rep'], true);

                    // store KPI value of rep in array using their rep-name as key
                    foreach ($d['rep'] as $rep) {
                        $repName = $rep['rep_name'];
                        $row['rep'][$repName] = $rep['kpi_value'];
                    }
                }   
            } else {
                if ($row['user_id'] == $d['user_id'] && $row['kpi_description'] == $d['kpi_description']) {
                    // for each user, we accumulate the kpi results and targets for each kpi_description from different dates 
                    $row['result'] = $row['result'] + $d['result'];
                    $row['target'] = $row['target'] + $d['target'];

                    if (!empty($d['rep'])) {
                        $d['rep'] = json_decode($d['rep'], true);

                        foreach ($d['rep'] as $rep) {
                            $repName = $rep['rep_name'];

                            if (array_key_exists($repName, $row['rep'])) {
                                // if the rep-name already exist, then accumulate the rep's KPI values
                                $row['rep'][$repName] = $row['rep'][$repName] + $rep['kpi_value'];
                            } else {
                                // store KPI value of rep in array using their rep-name as key
                                $row['rep'][$repName] = $rep['kpi_value'];
                            }
                        }
                    }
                } else {
                    // different user or kpi_description so push existing row into array
                    // then clear row to store next user's kpi
                    array_push($userKpis, $row);
                    unset($row);

                    // new row for different user
                    $row['user_id'] = $d['user_id'];
                    $row['user_name'] = $d['fname'] . " " . $d['lname'];
                    $row['kpi_description'] = $d['kpi_description'];
                    $row['result'] = $d['result'];
                    $row['target']= $d['target'];
                    $row['target_type']= $d['target_type'];
                    $row['rep'] = [];

                    // if not empty string nor null
                    if (!empty($d['rep'])) {
                        $d['rep'] = json_decode($d['rep'], true);

                        foreach ($d['rep'] as $rep) {
                            $repName = $rep['rep_name'];
                            $row['rep'][$repName] = $rep['kpi_value'];
                        }
                    }   
                }
            }
        }
        array_push($userKpis, $row); // push last row

        $kpiDescripHeader = array_unique(array_column($kpis, 'kpi_description'));

        $kpiGroupbyUser = [];
        $row2 = [];
        
        // group kpis by user
        foreach ($userKpis as $key=>$d) {
           
            if (empty($row2)) {
                // 1st iteration so create 1st row
                $row2[] = $d;
            } else {
                if ($row2[0]['user_id'] == $d['user_id']) {
                    $row2[] = $d;
                } else {
                    // different user so push existing row into array
                    // then clear row to store next user
                    array_push($kpiGroupbyUser, $row2);
                    unset($row2); 
                    $row2[] = $d; // new user so it should be in a different row from previous row
                }    
            }
        }
        array_push($kpiGroupbyUser, $row2); // push last row

        return [
            'userKpis' => $kpiGroupbyUser,
            'kpiDescripHeader' => $kpiDescripHeader
        ];
    }

}