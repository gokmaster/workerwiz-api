<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use App\Unverifieduser;
use App\Company;
use App\Role;
use App\Roletask;
use App\UserInvite;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'emp_id', 'company_id', 'fname', 'lname', 'email', 'personal_email', 'home_phone', 'mobile_phone', 'streetaddress', 'city', 'dob', 'marital_status', 'tin_number', 'fnpf_number', 
        'bank_name', 'bank_account_name', 'bank_account_number', 'bsb_number',
        'emergency_contact_name', 'emergency_contact_relationship', 'emergency_contact_phone', 'emergency_contact_phone2', 
        'emergency_contact_streetaddress', 'emergency_contact_city', 'medical_conditions', 'job_title_id',
        'role_id', 'department_id', 'cug_number', 'phone_extension', 'did_number', 'emp_startdate', 'emp_enddate', 'work_starttime', 'work_endtime', 
        'weekly_normal_hours', 'hourly_rate', 'allowed_apps', 'password', 'profilepic', 'profile_view', 'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function userAll($companyId) {
        return DB::table($this->table)
            ->select('users.*', 'role.role_name as role', 'department.department_name', 'job_title.job_title')
            ->leftjoin('role', 'users.role_id', '=', 'role.id')
            ->leftjoin('department', 'users.department_id', '=', 'department.id')
            ->leftjoin('job_title', 'users.job_title_id', '=', 'job_title.id')
            ->where('users.status', 'active')
            ->where('users.company_id', $companyId)
            ->orderBy('users.fname', 'ASC')
            ->get();
    }

    public function userFetch($userId, $companyId) {
        return DB::table($this->table)
            ->select( 'users.id', 'users.emp_id', 'users.fname', 'users.lname', 'users.email', 'users.personal_email', 'users.home_phone', 'users.mobile_phone', 'users.streetaddress', 
            'users.city', 'users.dob', 'users.marital_status', 'users.tin_number', 'users.fnpf_number', 
            'users.bank_name', 'users.bank_account_name', 'users.bank_account_number', 'users.bsb_number',
            'users.emergency_contact_name', 'users.emergency_contact_relationship', 'users.emergency_contact_phone', 'users.emergency_contact_phone2', 
            'users.emergency_contact_streetaddress', 'users.emergency_contact_city', 'users.medical_conditions', 'users.job_title_id',
            'users.role_id', 'users.department_id', 'users.cug_number', 'users.phone_extension', 'users.did_number', 'users.emp_startdate', 
            'users.emp_enddate', 'users.work_starttime', 'users.work_endtime', 'weekly_normal_hours',
            'users.hourly_rate', 'users.allowed_apps', 'users.profilepic', 'users.profile_view', 
            'role.role_name as role', 'department.department_name', 'job_title.job_title')
            ->leftjoin('role', 'users.role_id', '=', 'role.id')
            ->leftjoin('department', 'users.department_id', '=', 'department.id')
            ->leftjoin('job_title', 'users.job_title_id', '=', 'job_title.id')
            ->where('users.id', $userId)
            ->where('users.company_id', $companyId)
            ->get();
    }

    public function userCreate($data) {
        $empId = 10221000; // Emp-ID starts at 10221000

        $maxEmpId = User::max('emp_id');

        if ($maxEmpId >= $empId) {
            // if $maxEmpId is >= $empId, that means records already exist in users table
            $empId = $maxEmpId + 1; 
        }

        $data['emp_id'] = $empId;
        return User::create($data)->id;
    }

    public function userAndCompanyCreate($verificationCode) {
        DB::transaction(function()  use ($verificationCode) {
            $data = Unverifieduser::select('fname', 'lname', 'email', 'password', 'company_name', 'timezone')
                        ->where('verification_code', $verificationCode)->first();
            $data = json_decode(json_encode($data),true); // convert object to array

            Unverifieduser::where('verification_code', $verificationCode)->update(['verified'=> 1]);

            $companyData = [
                'company_name' => $data['company_name'],
                'timezone' => $data['timezone']
            ];
            $companyId = Company::create($companyData)->id; // create record for new company
            $data['company_id'] = $companyId;

            // fetch preset roles
            $roles = Role::select('id','role_name', 'codename')->where('company_id', 0)->get(); // company_id=0 is just a dummy company to store preset roles.
            $roles = json_decode(json_encode($roles),true); // convert object to array
            
            // assign the preset roles to the company just created above
            foreach ($roles as $role) {
                $role['company_id'] = $companyId; // instead of company_id=0, use the company's company_id 
                $roletasks = Roletask::select('task_id')->where('role_id', $role['id'])->get();
                $roletasks = json_decode(json_encode($roletasks),true); // convert object to array
                unset($role['id']); // remove existing id

                $newRoleId = Role::create($role)->id; // create roles for the new company

                foreach ($roletasks as $rt) {
                    $rt['role_id'] = $newRoleId; // instead of the role_ids of the preset roles, use the role_id of the newly created roles of the new company
                    Roletask::create($rt);
                }
            }

            $superUserRole = Role::select('id')->where('codename', 'owner')->where('company_id', $companyId)->first();
            $superUserRole = json_decode(json_encode($superUserRole),true); // convert object to array
            $data['role_id'] = $superUserRole['id']; // assign the 'owner' role to this new user
            $userId = $this->userCreate($data); // create new user record
        });
    }

    public function invitedUserCreate($data) {
        DB::transaction(function() use ($data) {
            $user = UserInvite::select('email','emp_startdate','work_starttime','work_endtime','weekly_normal_hours','hourly_rate','role_id','company_id')
                        ->where('verification_code', $data['verification_code'])->first();
            $user = json_decode(json_encode($user),true); // convert object to array

            $insert = array_merge($data, $user);
            $userId = $this->userCreate($insert); // create new user record

            UserInvite::where('verification_code', $data['verification_code'])->update(['account_created'=>1]);
        });
    }
}
