<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class Kpigroupdetail extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'kpi_group_detail';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'kpi_group_id', 'kpi_detail_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function edit($kpiGroupId, $kpiDetailIds = array() ) {
        DB::transaction(function()  use ($kpiGroupId, $kpiDetailIds) {
            DB::table('kpi_group_detail')->where('kpi_group_id', $kpiGroupId)->delete();

            $data = [];

            foreach ($kpiDetailIds as $kpiDetailId) {
                $row = [
                    'kpi_group_id' => $kpiGroupId,
                    'kpi_detail_id' => $kpiDetailId,
                ];
                array_push($data, $row);
            }

            DB::table('kpi_group_detail')->insert($data);
        });
    }

   
}
