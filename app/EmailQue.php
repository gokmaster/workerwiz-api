<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class EmailQue extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'email_que';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject', 'mail_to', 'mail_from', 'sent', 'email_campaign_id', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function fetchUnsentBatch() {
        return DB::table($this->table)
                    ->select('email_que.*', 'email_campaign.content', 'email_campaign.template_name', 'email_campaign.content_type', 
                    'email_campaign.bg_image_url','email_campaign.privacy_policy_url','email_campaign.website_url')
                    ->leftjoin('email_campaign', 'email_que.email_campaign_id', '=' , 'email_campaign.id')
                    ->where('email_que.sent', 0)
                    ->orderby('email_que.id', 'ASC')
                    ->take(1) // fetch maximum of 50 records
                    ->get();
    }
}
