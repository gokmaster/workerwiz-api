<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use App\CustomfieldValue;
use App\Customfield;

class Payroll extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'payroll';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'emp_id', 'emp_name', 'has_commission', 'total_hours_worked', 'hourly_rate', 'gross_wages', 
        'superannuation_deduction', 'tax_deduction', 'total_payable', 'payperiod_start', 'payperiod_end', 
        'payroll_excel_upload_id', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function payrollWithCustomFieldsCreate($data = []) {
        $customFields = $data['customfields'];
        unset($data['customfields']); // remove customfields from array

        DB::transaction(function()  use ($data, $customFields) {
            $id = Payroll::create($data)->id;

            foreach ($customFields as $inputCodename=>$value) {
                CustomfieldValue::create([
                    'linkedtable' => 'payroll',
                    'linkedtable_id' => $id,
                    'input_codename' => $inputCodename,
                    'input_value' => $value,
                ]);
            }
        });
    }

    public function fetchRecordsForUploadedExcel($payrollExcelUploadId, $payperiodEnd, $companyId) {
        $year = date('Y', strtotime($payperiodEnd));
        $yearStart = $year . '-01-01';
        $qry = "SELECT payroll.*, users.id as user_id, users.email, users.emp_startdate, users.tin_number,
                users.fnpf_number, users.weekly_normal_hours, f.superannuation_deduct_ytd, t.days_earned
                FROM payroll 
                LEFT JOIN users ON payroll.emp_id = users.emp_id 
                LEFT JOIN (
                    SELECT user_id, COUNT(DISTINCT DATE(checkin)) as days_earned from timesheet
                    WHERE (DATE(checkin) BETWEEN '$yearStart' AND '$payperiodEnd')
                    GROUP BY user_id
                ) t ON users.id = t.user_id
                JOIN (
                    SELECT emp_id, SUM(superannuation_deduction) AS superannuation_deduct_ytd from payroll 
                    WHERE (payperiod_end BETWEEN '$yearStart' AND '$payperiodEnd') 
                    GROUP BY emp_id
                ) f ON payroll.emp_id = f.emp_id
                WHERE payroll.payroll_excel_upload_id = $payrollExcelUploadId
                AND payroll.company_id = $companyId";

        $payrollEmployees = DB::select( DB::raw($qry) );
        $payrollEmployees = json_decode(json_encode($payrollEmployees),true); // convert object to array

        foreach ($payrollEmployees as $i=>$payrollEmployee) {
            if(CustomfieldValue::where('linkedtable','payroll')->where('linkedtable_id', $payrollEmployee['id'])->exists()) {
                $payrollEmployees[$i]['customfields'] = $this->employeePayrollCustomFields($payrollEmployee['id'], $companyId);
            }
        }

        return $payrollEmployees;
    } 
    
    private function employeePayrollCustomFields($payrollId, $companyId) {
        $customField = Customfield::select('customfield.field_label','customfield_value.input_value','company_customfield.form_section','customfield_value.linkedtable_id')
                        ->join('company_customfield', 'customfield.input_codename', '=', 'company_customfield.input_codename')
                        ->leftjoin('customfield_value', function($join) use ($payrollId) {
                            $join->on('customfield.input_codename', '=', 'customfield_value.input_codename');
                            $join->where('customfield_value.linkedtable_id',$payrollId);
                        })
                        ->where('customfield.form', 'payroll')
                        ->where('company_customfield.company_id', $companyId)
                        ->orderBy('company_customfield.form_section_sort', 'ASC')
                        ->orderBy('company_customfield.field_sort', 'ASC')
                        ->get(); 
        $customField = json_decode(json_encode($customField),true); // convert object to array

        $customFieldGrouped = [];

        if ($customField) {
            foreach($customField as $cf) {
                $section = $cf['form_section'];
                $key = $cf['field_label'];
                $customFieldGrouped[$section][$key] = $cf['input_value'];
            }
        }
        
        return $customFieldGrouped;
    } 
}
