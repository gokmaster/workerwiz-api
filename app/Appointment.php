<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class Appointment extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'appointment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rep_id', 'day', 'appointment_date', 'appointment_time', 'business_name', 'client_type', 'phone', 'mobile', 'street_address', 'city', 
        'zipcode', 'country', 'fname', 'lname', 'bill_amount', 'email', 'agent_notes', 'user_id', 'status','first_for_day',
        'confirmer_user_id', 'confirmer_notes', 'drop_reason', 'ice_reason', 'approver_notes', 'reschedule_appointment_id', 'archived', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function fetchPast6MonthsAndNext6MonthsByRep($companyId) {
        $qry = "SELECT appointment.*, (time_to_sec(appointment.appointment_time) / (60 * 60)) AS appointment_time_dec, 
                rep.fname as rep_fname, rep.lname as rep_lname, rep.zipcode as rep_zipcode
                FROM appointment
                LEFT JOIN rep ON appointment.rep_id = rep.id
                WHERE 
                    (appointment_date >= date_sub(now(), interval 6 month)
                AND 
                    appointment_date <= date_add(now(), interval 6 month))
                AND
                    appointment.status <> 'dropped'
                AND
                    appointment.status <> 'iced'
                AND
                    appointment.archived = 0
                AND
                    appointment.company_id = $companyId
                ORDER BY rep.id, appointment.appointment_date ASC, appointment.appointment_time ASC"; 

        return DB::select( DB::raw($qry) );
    }

    public function fetchLast2WeeksAndNext6MonthsToConfirm($userId,$companyId) {
        $qry = "SELECT appointment.*, rep.fname as rep_fname, rep.lname as rep_lname, users.fname as confirmer_fname, users.lname as confirmer_lname 
                FROM appointment
                LEFT JOIN rep ON appointment.rep_id = rep.id
                LEFT JOIN users ON appointment.confirmer_user_id = users.id
                WHERE 
                    (appointment_date >= date_sub(now(), interval 2 week)
                AND 
                    appointment_date <= date_add(now(), interval 6 month))
                AND
                    appointment.status <> 'iced'
                AND
                    appointment.status <> 'blocked'
                AND
                    appointment.archived = 0
                AND
                    appointment.company_id = $companyId
                ORDER BY appointment.appointment_date ASC, appointment.appointment_time ASC"; 

        return DB::select( DB::raw($qry) );
    }

    public function fetchLast2WeeksAndNext6MonthsForConfirmer($userId,$companyId) {
        $qry = "SELECT appointment.*, rep.fname as rep_fname, rep.lname as rep_lname, users.fname as confirmer_fname, users.lname as confirmer_lname 
                FROM appointment
                LEFT JOIN rep ON appointment.rep_id = rep.id
                LEFT JOIN users ON appointment.confirmer_user_id = users.id
                WHERE 
                    (appointment_date >= date_sub(now(), interval 2 week)
                AND 
                    appointment_date <= date_add(now(), interval 6 month))
                AND
                    appointment.confirmer_user_id = $userId
                AND
                    appointment.status <> 'iced'
                AND
                    appointment.status <> 'blocked'
                AND
                    appointment.archived = 0
                AND
                    appointment.company_id = $companyId
                ORDER BY appointment.appointment_date ASC, appointment.appointment_time ASC"; 

        return DB::select( DB::raw($qry) );
    }

    public function fetchIcedAppointments($companyId) {
        $qry = "SELECT appointment.*, rep.fname as rep_fname, rep.lname as rep_lname FROM appointment
                LEFT JOIN rep ON appointment.rep_id = rep.id
                WHERE 
                    (appointment_date >= date_sub(now(), interval 12 month)
                AND 
                    appointment_date <= date_add(now(), interval 6 month))
                AND
                    appointment.status = 'iced'
                AND
                    appointment.archived = 0
                AND
                    appointment.company_id = $companyId
                ORDER BY appointment.appointment_date DESC, appointment.appointment_time DESC"; 

        return DB::select( DB::raw($qry) );
    }

    public function fetchDroppedAppointments($companyId) {
        $qry = "SELECT appointment.*, rep.fname as rep_fname, rep.lname as rep_lname FROM appointment
                LEFT JOIN rep ON appointment.rep_id = rep.id
                WHERE 
                    (appointment_date >= date_sub(now(), interval 12 month)
                AND 
                    appointment_date <= date_add(now(), interval 6 month))
                AND
                    appointment.status = 'dropped'
                AND
                    appointment.archived = 0
                AND
                    appointment.company_id = $companyId
                ORDER BY appointment.appointment_date DESC, appointment.appointment_time DESC"; 

        return DB::select( DB::raw($qry) );
    }

    public function fetchArchivedAppointments($companyId) {
        $qry = "SELECT appointment.*, rep.fname as rep_fname, rep.lname as rep_lname FROM appointment
                LEFT JOIN rep ON appointment.rep_id = rep.id
                WHERE 
                    (appointment_date >= date_sub(now(), interval 12 month)
                AND 
                    appointment_date <= date_add(now(), interval 6 month))
                AND
                    appointment.archived = 1
                AND
                    appointment.company_id = $companyId
                ORDER BY appointment.appointment_date DESC, appointment.appointment_time DESC"; 

        return DB::select( DB::raw($qry) );
    }

    public function fetch($appointmentId, $companyId) {
        return DB::table('appointment')
                ->select('appointment.*', 'users.fname as agent_fname', 'users.lname as agent_lname', 'rep.fname as rep_fname', 'rep.lname as rep_lname')
                ->leftjoin('users', 'appointment.user_id', '=' , 'users.id')
                ->leftjoin('rep', 'appointment.rep_id', '=' , 'rep.id')
                ->where([ ['appointment.id',$appointmentId],['appointment.company_id', $companyId] ])
                ->first();
    }

    public function timeslotBlocked($appointmentDate, $appointmentTime, $repId) {
        $qry = "SELECT count(id) as num FROM appointment WHERE 
                appointment_date = '$appointmentDate' 
                AND HOUR(appointment_time) = HOUR('$appointmentTime') 
                AND rep_id = $repId
                AND status = 'blocked';"; 

        return DB::select( DB::raw($qry) );
    }

}
