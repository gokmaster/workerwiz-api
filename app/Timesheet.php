<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use App\Loginlog;
use App\User;

class Timesheet extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'timesheet';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'checkin', 'checkout', 'type', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function checkinTimeInsert($userId) {
        date_default_timezone_set(env('APP_TIMEZONE'));

        $d = Timesheet::select('id', 'checkin')->where(['user_id'=> $userId, 'type' => 'normal'])->latest('checkin')->first();
        
        $user = User::select('company_id')->where('id',$userId)->first();
        $user = json_decode(json_encode($user),true); // convert object to array

        if ($d == null || strtotime($d->checkin) + 54000 < time() ) {
           // if latest checkin-time was more than 15 hours ago, then create new timesheet entry (3600 = 1 hour)
            $data = [
               'user_id' => $userId,
               'company_id' => $user['company_id'],
               'checkin' => date('Y-m-d H:i:s'),
               'type' => 'normal',
            ];

            Timesheet::create($data);
        }
    }

    public function checkoutTimeUpdate($userId) {
        date_default_timezone_set(env('APP_TIMEZONE'));
        
        $data = [
            'checkout' => date('Y-m-d H:i:s'),
        ];

        $d = Timesheet::select('id')->where(['user_id'=> $userId, 'type' => 'normal'])->latest('checkin')->first();

        Timesheet::find($d->id)->update($data);
    }

    public function currentWeekRecords($user_id, $timezoneUtcOffset) {
        $qry = "SELECT * FROM  $this->table 
                WHERE YEARWEEK(CONVERT_TZ(checkin,'+00:00', '$timezoneUtcOffset'), 1) = YEARWEEK(CURDATE(), 1)
                AND `user_id` = $user_id
                ORDER BY `checkin`";

        return DB::select( DB::raw($qry) );
    }

    public function lastWeekTilYesterdayRecords($user_id, $timezoneUtcOffset) {
        $qry = "SELECT * FROM  $this->table 
                WHERE YEARWEEK(CONVERT_TZ(checkin,'+00:00', '$timezoneUtcOffset'), 1) = YEARWEEK(NOW() - INTERVAL 1 WEEK, 1)
                AND `user_id` = $user_id"; // last week records

        $qry .= " UNION 
                SELECT * FROM  $this->table
                WHERE checkin < DATE(NOW()) AND CONVERT_TZ(checkin,'+00:00', '$timezoneUtcOffset') >= DATE(SUBDATE(now(), weekday(NOW())))
                AND `user_id` = $user_id
                ORDER BY `checkin`"; // current week records uptil yesterday

        return DB::select( DB::raw($qry) );
    }

    public function lastWeekRecordsForAllUsers($companyId, $timezoneUtcOffset) {
        // week starts on Monday and ends on sunday
        // times are converted to user's timezone
        $qry = "SELECT users.id,users.fname,users.lname, users.work_starttime, users.work_endtime, department.department_name, t.*
                FROM users
                LEFT JOIN department ON users.department_id = department.id
                LEFT JOIN (
                SELECT CONVERT_TZ(timesheet.checkin,'+00:00', '$timezoneUtcOffset') AS checkin, 
                    CONVERT_TZ(timesheet.checkout,'+00:00', '$timezoneUtcOffset') AS checkout, user_id FROM timesheet
                WHERE YEARWEEK(CONVERT_TZ(timesheet.checkin,'+00:00', '$timezoneUtcOffset'), 1) = YEARWEEK(NOW() - INTERVAL 1 WEEK, 1)
                AND (timesheet.type = 'normal' OR timesheet.type IS NULL)
                ) t ON users.id = t.user_id
                WHERE users.status = 'active'
                AND users.company_id = $companyId
                ORDER BY users.fname, users.lname, t.checkin"; // last week records

        return DB::select( DB::raw($qry) );
    }
}
