<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use App\CustomfieldValue;

class Sale extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'sale';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'lname', 'phone', 'email', 'address1', 'address2', 'city', 'state', 'zipcode', 'country', 
        'postal_address1','postal_address2','postal_city','postal_state','postal_zipcode','postal_country','preference','price_comfort',
        'campaign_id','description','sku','quantity','saleprice','notes','creditcard_number',
        'cardholder_name','card_expiry_date','cvv','sale_agent', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function saleWithCustomFieldsCreate($saleData = [], $customFields = [] ) {
        DB::transaction(function()  use ($saleData, $customFields) {
            $saleId = Sale::create($saleData)->id;

            foreach ($customFields as $inputCodename=>$value) {
                CustomfieldValue::create([
                    'linkedtable' => 'sale',
                    'linkedtable_id' => $saleId,
                    'input_codename' => $inputCodename,
                    'input_value' => $value,
                ]);
            }
        });
    }

    public function saleWithCustomFieldsEdit($saleData = [], $customFields = [], $saleId) {
        DB::transaction(function()  use ($saleData, $customFields, $saleId) {
            Sale::find($saleId)->update($saleData);

            foreach ($customFields as $inputCodename=>$value) {
                if (CustomfieldValue::where(['linkedtable'=>'sale','linkedtable_id'=>$saleId,'input_codename'=>$inputCodename])->exists()) {
                    CustomfieldValue::where(['linkedtable'=>'sale','linkedtable_id'=>$saleId,'input_codename'=>$inputCodename])
                                    ->update(['input_value' => $value]);
                } else {
                    CustomfieldValue::create([
                        'linkedtable' => 'sale',
                        'linkedtable_id' => $saleId,
                        'input_codename' => $inputCodename,
                        'input_value' => $value,
                    ]);
                }
            }
        });
    }

    public function salesOfYear($year, $companyId) {
        return DB::table($this->table)
            ->select('sale.*','users.fname as sale_agent_fname','users.lname as sale_agent_lname', 'campaign.campaign_name')
            ->leftjoin('campaign', 'sale.campaign_id', '=', 'campaign.id')
            ->leftjoin('users', 'sale.sale_agent', '=', 'users.id')
            ->whereYear('sale.created_at', '=', $year)
            ->where('sale.company_id', $companyId)
            ->orderBy('sale.created_at','DESC')
            ->get();
    }

    public function salesOfAgentForYear($userId, $year, $companyId) {
        return DB::table($this->table)
            ->select('sale.*','users.fname as sale_agent_fname','users.lname as sale_agent_lname', 'campaign.campaign_name')
            ->leftjoin('campaign', 'sale.campaign_id', '=', 'campaign.id')
            ->leftjoin('users', 'sale.sale_agent', '=', 'users.id')
            ->whereYear('sale.created_at', '=', $year)
            ->where('sale.sale_agent', $userId)
            ->orderBy('sale.created_at','DESC')
            ->get();
    }

    public function saleFetch($saleId) {
        return Sale::select('sale.*','campaign.campaign_name')->where('sale.id', $saleId)
                            ->leftjoin('campaign', 'sale.campaign_id', '=', 'campaign.id')->first();
    }

    public function saleWithCustomFieldsFetch($saleId, $companyId) {
        $sale = $this->saleFetch($saleId);
        $sale = json_decode(json_encode($sale),true); // convert object to array

        $customField = Customfield::select('customfield.field_label','customfield_value.input_value','company_customfield.form_section','customfield_value.linkedtable_id')
                        ->join('company_customfield', 'customfield.input_codename', '=', 'company_customfield.input_codename')
                        ->leftjoin('customfield_value', function($join) use ($saleId) {
                            $join->on('customfield.input_codename', '=', 'customfield_value.input_codename');
                            $join->where('customfield_value.linkedtable_id',$saleId);
                        })
                        ->where('customfield.form', 'sale')
                        ->where('company_customfield.company_id', $companyId)
                        ->orderBy('company_customfield.form_section_sort', 'ASC')
                        ->orderBy('company_customfield.field_sort', 'ASC')
                        ->get(); 
        $customField = json_decode(json_encode($customField),true); // convert object to array

        if ($customField) {
            foreach($customField as $cf) {
                $section = $cf['form_section'];
                $key = $cf['field_label'];
                $sale['customfields'][$section][$key] = $cf['input_value'];
            }
        }

        return $sale;
    }
}
