<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProfileAttachment extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'profile_attachment';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description', 'attachment_type_codename', 'file', 'date_applied', 'user_id', 'company_id', 'deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function profileAttachmentOfUser($userId) {
        return DB::table('profile_attachment')
                ->select('profile_attachment.*', 'attachment_type.title')
                ->leftjoin('attachment_type', 'profile_attachment.attachment_type_codename', '=' , 'attachment_type.codename')
                ->where('profile_attachment.user_id', $userId)
                ->where('attachment_type.group', 'profile')
                //->where('profile_attachment.created_at', '>=', Carbon::now()->subDays(730)->toDateTimeString()) // select records that are less than 2 years old
                ->where('profile_attachment.deleted', 0)
                ->orderBy('profile_attachment.updated_at', 'DESC')
                ->get();
    }
}
