<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use App\CustomfieldValue;
use App\Customfield;

class Customer extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'customer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'lname', 'phone', 'email', 'address1', 'address2', 'city', 'state', 'zipcode', 'country', 
        'postal_address1','postal_address2','postal_city','postal_state','postal_zipcode','postal_country',
        'preference','price_comfort','creditcard_number','cardholder_name','card_expiry_date','cvv',
        'status','campaign_id','company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function customerWithCustomFieldsCreate($customerData = [], $customFields = [] ) {
        DB::transaction(function()  use ($customerData, $customFields) {
            $customerId = Customer::create($customerData)->id;

            foreach ($customFields as $inputCodename=>$value) {
                CustomfieldValue::create([
                    'linkedtable' => 'customer',
                    'linkedtable_id' => $customerId,
                    'input_codename' => $inputCodename,
                    'input_value' => $value,
                ]);
            }
        });
    }

    public function customerWithCustomFieldsEdit($customerData = [], $customFields = [], $customerId) {
        DB::transaction(function()  use ($customerData, $customFields, $customerId) {
            Customer::find($customerId)->update($customerData);

            foreach ($customFields as $inputCodename=>$value) {
                if (CustomfieldValue::where(['linkedtable'=>'customer','linkedtable_id'=>$customerId,'input_codename'=>$inputCodename])->exists()) {
                    CustomfieldValue::where(['linkedtable'=>'customer','linkedtable_id'=>$customerId,'input_codename'=>$inputCodename])
                                    ->update(['input_value' => $value]);
                } else {
                    CustomfieldValue::create([
                        'linkedtable' => 'customer',
                        'linkedtable_id' => $customerId,
                        'input_codename' => $inputCodename,
                        'input_value' => $value,
                    ]);
                }
            }
        });
    }

    public function customerWithCustomFieldsFetch($customerId, $companyId) {
        $customer = Customer::where('id', $customerId)->first();
        $customer = json_decode(json_encode($customer),true); // convert object to array

        $customField = Customfield::select('customfield.field_label','customfield_value.input_value','company_customfield.form_section','customfield_value.linkedtable_id')
                        ->join('company_customfield', 'customfield.input_codename', '=', 'company_customfield.input_codename')
                        ->leftjoin('customfield_value', function($join) use ($customerId) {
                            $join->on('customfield.input_codename', '=', 'customfield_value.input_codename');
                            $join->where('customfield_value.linkedtable_id',$customerId);
                        })
                        ->where('customfield.form', 'customer')
                        ->where('company_customfield.company_id', $companyId)
                        ->orderBy('company_customfield.form_section_sort', 'ASC')
                        ->orderBy('company_customfield.field_sort', 'ASC')
                        ->get(); 
        $customField = json_decode(json_encode($customField),true); // convert object to array

        if ($customField) {
            foreach($customField as $cf) {
                $section = $cf['form_section'];
                $key = $cf['field_label'];
                $customer['customfields'][$section][$key] = $cf['input_value'];
            }
        }

        return $customer;
    }

}
