<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class Teamusers extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'team_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_id', 'user_id', 'member_type'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function teamUsers($team_id, $companyId) {
        return DB::table($this->table)
            ->select('team_users.team_id', 'users.id as user_id', 'users.fname', 'users.lname', 'team_users.member_type')
            ->rightjoin('users', 'team_users.user_id', '=' , 'users.id')
            //->whereNull('team_id')
            //->orwhere('team_id', $team_id)
            ->where('users.status', 'active')
            ->where('users.company_id', $companyId)
            ->orderby('users.fname', 'ASC')
            ->get();
    }

    public function teamMembers($team_id, $companyId) {
        return DB::table($this->table)
            ->select('team_users.team_id', 'users.id as user_id', 'users.fname', 'users.lname', 'team_users.member_type', 'team.team_name')
            ->leftjoin('users', 'team_users.user_id', '=' , 'users.id')
            ->join('team', 'team_users.team_id', '=', 'team.id')
            ->where('team_users.team_id', $team_id)
            ->where('team.company_id', $companyId)
            ->where('users.status', 'active')
            ->orderby('users.fname', 'ASC')
            ->get();
    }

    public function teamMembersEdit($members = array(), $teamId) {
        DB::transaction(function()  use ($members, $teamId) {
            Teamusers::where('team_id', $teamId)->delete();
            Teamusers::insert($members);
        });
    }

    public function teamMembersOfUser($userId, $companyId) {
        $d = Teamusers::select('team_id')->where('user_id', $userId)->first();

        return DB::table($this->table)
            ->select('users.id as user_id', 'users.fname', 'users.lname')
            ->leftjoin('users', 'team_users.user_id', '=' , 'users.id')
            ->where('team_users.team_id', $d['team_id'])
            //->where('team_users.user_id', '!=' , $userId)
            ->where('users.status', 'active')
            ->where('users.company_id', $companyId)
            ->orderby('users.fname', 'ASC')
            ->get();
    }
   
}
