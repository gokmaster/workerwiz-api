<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class UserInvite extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'user_invite';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'emp_startdate', 'work_starttime', 'work_endtime', 'weekly_normal_hours', 'hourly_rate', 
        'role_id', 'account_created', 'verification_code', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function companyFetch($verificationCode) {
        return DB::table($this->table)
            ->select( 'company.company_name', 'user_invite.email', 'user_invite.verification_code')
            ->join('company', 'user_invite.company_id', '=', 'company.id')
            ->where('verification_code', $verificationCode)
            ->first();
    }

}
