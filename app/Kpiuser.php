<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class Kpiuser extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'kpi_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'kpi_group_id', 'kpi_detail_id', 'kpi_description', 'target', 'target_type', 'result', 'kpi_date', 'rep'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function insert($data = array() ) {
        return DB::transaction(function()  use ($data) {
            $kpiEnddate = date('Y-m-d', strtotime($data['kpi_startdate'] . ' +4 day'));

            $count =  Kpiuser::where('user_id', $data['user_id'])
                                ->whereBetween('kpi_date', [$data['kpi_startdate'], $kpiEnddate])->count();
                   
            // if user has not been assigned KPI for that period
            if ($count == 0) {
                $kpiDetails = DB::table('kpi_detail')
                                ->select('kpi_detail.id as kpi_detail_id', 'kpi_detail.description as kpi_description', 'kpi_detail.target', 'kpi_detail.target_type')
                                ->join('kpi_group_detail', 'kpi_detail.id', '=' , 'kpi_group_detail.kpi_detail_id')
                                ->where('kpi_group_detail.kpi_group_id', $data['kpi_group_id'])
                                ->get();
                $kpiDetails = json_decode(json_encode($kpiDetails), true); // convert to array
    
                $kpiDate = $data['kpi_startdate'];

                // Loop for 5 days
                for ($x = 0; $x <= 4; $x++) {
                    $i = 0;
                    foreach ($kpiDetails as $k) {
                        $kpiDetails[$i]['user_id'] = (int) $data['user_id'];
                        $kpiDetails[$i]['kpi_group_id'] = (int) $data['kpi_group_id'];
                        $kpiDetails[$i]['kpi_date'] = $kpiDate;
                                    
                        $i++;
                    }
                    $kpiDate = date('Y-m-d', strtotime($kpiDate . ' +1 day'));
                    DB::table($this->table)->insert($kpiDetails);
                }

                return [
                    'already_exist' => false,
                ];
            } else {
                return [
                    'already_exist' => true,
                ];
            }
        });    
    }

    public function userKpiUpdate($data = []) {
        foreach ($data as $d) {
            Kpiuser::find($d->id)->update([
                'result' => $d->result,
                'rep' => $d->rep,
            ]);
        }
    }

    public function lastWeekTilYesterdayRecords($user_id) {
        $qry = "SELECT * FROM  $this->table 
                WHERE YEARWEEK(`kpi_date`, 1) = YEARWEEK(NOW() - INTERVAL 1 WEEK, 1)
                AND `user_id` = $user_id"; // last week records

        $qry .= " UNION 
                SELECT * FROM  $this->table
                Where kpi_date < DATE(NOW()) AND kpi_date >= DATE(SUBDATE(now(), weekday(NOW())))
                AND `user_id` = $user_id
                ORDER BY `kpi_description`, `kpi_date`"; // current week records uptil yesterday

        return DB::select( DB::raw($qry) );
    }

    public function kpiBetweenDates($data = []) {
        return DB::table('kpi_user')
            ->select('kpi_user.*', 'users.fname', 'users.lname')
            ->leftjoin('users', 'kpi_user.user_id', '=' , 'users.id')
            ->where('kpi_user.kpi_group_id', $data['kpi_group_id'])
            ->whereBetween('kpi_user.kpi_date', [$data['start_date'] , $data['end_date']])
            ->orderBy('users.fname', 'ASC')
            ->orderBy('kpi_user.user_id', 'ASC')
            ->orderBy('kpi_user.kpi_description', 'ASC')
            ->get();
    }
   
}
