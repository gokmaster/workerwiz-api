<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class Roletask extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'role_task';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'task_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    // fetch the task_ids of particular user's role
    public function userRoleTasks($userId) {
        return DB::table($this->table)
                    ->select('role_task.task_id', 'task.codename')
                    ->where('users.id' , $userId)
                    ->join('users', 'role_task.role_id', '=', 'users.role_id')
                    ->join('task', 'role_task.task_id', '=', 'task.id')
                    ->get();
    }

}
