<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use App\User;

class PasswordReset extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'password_reset';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'old_password', 'new_password', 'email', 'verification_code', 'resetted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function passwordChangeAfterConfirmation($verificationCode) {
        DB::transaction(function() use ($verificationCode) {
            // fetch new password
            $pwordReset = PasswordReset::where('verification_code',$verificationCode)->first();
            $pwordReset = json_decode(json_encode($pwordReset),true); // convert object to array

            $update = [
                'password' => $pwordReset['new_password']
            ];

            User::where('email', $pwordReset['email'])->update($update); // change user password to new one
            PasswordReset::where('verification_code',$verificationCode)->update(['resetted'=>1]); // mark password-reset-request as "resetted"
        });
    }
}
