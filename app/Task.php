<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class Task extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'task';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function fetchAll() {
        return DB::table($this->table)
                    ->orderBy('label', 'asc')
                    ->orderBy('task_name', 'asc')
                    ->get();
    }

    // fetch the menus of particular user's role
    public function roleMenu($user_id) {
       
        $data = DB::table($this->table)
                    ->select('task.menu_label', 'task.menu_icon', 'task.route_name', 'task.is_submenu', 'task.menu_header_label')
                    ->join('role_task' , 'task.id', '=', 'role_task.task_id')
                    ->join('users' , 'users.role_id' , '=' , 'role_task.role_id')
                    ->where([
                        'users.id' => $user_id,
                        'task.is_menu' => 1,
                        ])
                    ->orderBy('menu_sort', 'asc')
                    ->get();

        $menu_array = array();

        foreach ($data as $d) {
            if ( strcmp( $d->is_submenu , 0) === 0 ) {
                // if this is not a submenu, directly push it to the main array
                $d->has_submenu = 0;
                array_push($menu_array, $d);
            } else {
                // if it is a submenu, create an level-2-array with the menu_header_label as key and push submenu to this array
                $menu_array[$d->menu_header_label]['has_submenu'] = 1;

                if (!isset( $menu_array[$d->menu_header_label]['submenu'] ) ) { 
                    $menu_array[$d->menu_header_label]['submenu'] = array();
                }
                array_push($menu_array[$d->menu_header_label]['submenu'], $d);
            }
        }
        return $menu_array;
    }
}
