<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class SubscriptionPlanOrder extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'subscription_plan_order';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id', 'subscription_plan_no', 'subscription_period_days', 'subscription_period_months', 'user_quantity', 'payment_amount', 'paid_datetime', 'paid', 'created_by'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function allUnpaidFetch() {
        return DB::table('subscription_plan_order')
                ->select('subscription_plan_order.*', 'company.company_name','subscription_plan.title as subscription_plan')
                ->leftjoin('company', 'subscription_plan_order.company_id', '=' , 'company.id')
                ->leftjoin('subscription_plan', 'subscription_plan_order.subscription_plan_no', '=' , 'subscription_plan.subscription_plan_no')
                ->where('subscription_plan_order.paid', 0)
                ->whereNotNull('cvv')
                ->orderBy('subscription_plan_order.created_at', 'DESC')
                ->get();
    }

    public function userWhoCreatedOrder($subscripPlanOrderId) {
        return SubscriptionPlanOrder::select('subscription_plan_order.*','subscription_plan.title as subscription_plan','users.fname', 'users.lname','users.email')
                ->join('users', 'subscription_plan_order.created_by', '=' , 'users.id')
                ->leftjoin('subscription_plan', 'subscription_plan_order.subscription_plan_no', '=' , 'subscription_plan.subscription_plan_no')
                ->where('subscription_plan_order.id',$subscripPlanOrderId)
                ->first();
    }
}
