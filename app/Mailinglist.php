<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;
use App\Mailinggroup;

class Mailinglist extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'mailinglist';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'lname', 'email','tags','mailinggroup_id','company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function mailinglistAndMailingGroupCreate($data=[]) {
        DB::transaction(function()  use ($data) {
            $mailingGrpId = Mailinggroup::create([
                'title' => $data['title'],
                'company_id' => $data['company_id']
            ])->id;

            $mailinglist = json_decode($data['mailinglist'],true);

            foreach ($mailinglist as $ml) {
                Mailinglist::create([
                    'email' => $ml['email'],
                    'mailinggroup_id' => $mailingGrpId,
                    'company_id' => $data['company_id'],
                ]);
            }
        });
    }
}
