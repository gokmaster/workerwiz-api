<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\DB;

class MailConfig extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable;

    // Set a table name in database
    protected $table = 'mail_config';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'driver', 'host', 'port', 'from_address', 'from_name', 'encryption', 'username', 'password', 'codename', 'in_use', 'company_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    public function changeMailServer($mailConfigId) {
        DB::transaction(function()  use ($mailConfigId) {
            DB::table($this->table)->update(['in_use'=>0]);
            MailConfig::find($mailConfigId)->update(['in_use'=>1]);
        });
    }
   
}
