<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
 
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('/', function () use ($router) {
        return "API is working.";
    });

    // routes within this group will require valid access-token to be passed in header in order to be accessible 
    $router->group(['middleware' => 'auth'], function () use ($router) {
        // UserController
        $router->post('/user/create','UserController@create');
        $router->post('/user/edit','UserController@edit');
        $router->post('/user/delete','UserController@delete');
        $router->post('/user/unarchive','UserController@unarchive');        
        $router->post('/user/details', 'UserController@getUserDetails');
        $router->post('/users', 'UserController@allUsers');
        $router->post('/users/ids', 'UserController@allUsersIds');
        $router->post('/user/password/change','UserController@passwordChange');
        $router->post('/users/archived', 'UserController@allArchivedUsers');
        $router->post('/user/invite/create', 'UserController@userInviteCreate');
                      
        // TimesheetController
        $router->post('timesheet/add','TimesheetController@add');
        $router->post('timesheet/edit','TimesheetController@edit');
        $router->post('timesheet/currentweek','TimesheetController@currentWeekRecords');
        $router->post('timesheet/lastweek','TimesheetController@lastWeekRecords');
        $router->post('timesheet/users/all','TimesheetController@allUsersRecords');
        $router->post('timesheet/approve/all','TimesheetController@approveAll');

        // TeamControlller
        $router->post('team/create','TeamController@create');
        $router->post('team/all','TeamController@all');
        $router->post('team/users','TeamController@teamUsers');
        $router->post('team/members','TeamController@teamMembers');
        $router->post('team/members/edit','TeamController@membersEdit');
        $router->post('team/members/ofuser','TeamController@teamMembersOfUser');
       
        // RoleController
        $router->get('task/all','RoleController@tasksAll');
        $router->post('role/all','RoleController@all');
        $router->post('role/create','RoleController@create');
        $router->post('role/edit','RoleController@edit');
        $router->post('role/menu','RoleController@roleMenu');
        $router->post('role/fetch','RoleController@roleFetch');
        $router->post('role/usertasks','RoleController@allowedTasksOfUser'); // task_ids of permitted tasks for logged-in user
        $router->post('role/userpermissions','RoleController@userPermissions');
        $router->post('task/fetch','RoleController@taskFetch');

        // KpiController
        $router->post('kpi/group/create','KpiController@kpiGroupCreate');
        $router->post('kpi/group/all','KpiController@kpiGroupAll');
        $router->post('kpi/detail/create','KpiController@kpiDetailCreate');
        $router->post('kpi/detail/all','KpiController@kpiDetailAll');
        $router->post('kpi/assign/togroup','KpiController@kpiAssignToGroup');
        $router->post('kpi/ofgroup','KpiController@kpiOfGroup');
        $router->post('kpi/assign/touser','KpiController@kpiAssignToUser');
        $router->post('kpi/userkpis','KpiController@userKpisForEntry');
        $router->post('kpi/userkpi/update','KpiController@userKpiUpdate');
        $router->post('kpi/report','KpiController@kpiBetweenDates');

        // PayrollController
        $router->post('payroll/excelupload/create','PayrollController@excelUploadAdd');
        $router->post('payroll/excelupload/edit','PayrollController@excelUploadEdit');
        $router->post('payroll/records/create','PayrollController@payrollRecordsAdd');
        $router->post('payroll/excelsuploaded/fetch','PayrollController@uploadedExcelsFetch');
        $router->post('payroll/records/fetch','PayrollController@payrollRecordsFetch');
        $router->post('payslip/create','PayrollController@payslipAdd');
        $router->post('payslip/ofuser','PayrollController@payslipOfUser');
        
        // HolidayController
        $router->post('holiday/create','HolidayController@create');
        $router->post('holiday/fetch','HolidayController@fetch');
        $router->post('holiday/edit','HolidayController@edit');
        $router->post('holiday/delete','HolidayController@delete');

        // DepartmentController
        $router->post('department/create','DepartmentController@create');
        $router->post('department/edit','DepartmentController@edit');
        $router->post('department/delete','DepartmentController@delete');
        $router->post('departments','DepartmentController@all');

        // JobtitleController
        $router->post('jobtitle/create','JobtitleController@create');
        $router->post('jobtitle/edit','JobtitleController@edit');
        $router->post('jobtitle/delete','JobtitleController@delete');
        $router->post('jobtitles','JobtitleController@all');
                
        // RepController
        $router->post('rep/create','RepController@create');
        $router->post('rep/edit','RepController@edit');
        $router->post('rep/delete','RepController@delete');
        $router->post('rep/all','RepController@all');
        $router->post('rep/fetch/byid/projectmanager','RepController@fetchRepByIdAndProjectManager');
        $router->post('rep/fetch/projectmanager','RepController@fetchProjectManager');

        // AppointmentController
        $router->post('appointment/create','AppointmentController@create');
        $router->post('appointment/edit','AppointmentController@edit');
        $router->post('appointment/fetch','AppointmentController@fetch');
        $router->post('appointment/fetch/forconfirmer','AppointmentController@fetchForConfirmer');
        $router->post('appointment/fetch/byrep','AppointmentController@fetchByRep'); 
        $router->post('appointment/fetch/iced','AppointmentController@fetchIcedAppointments'); 
        $router->post('appointment/fetch/dropped','AppointmentController@fetchDroppedAppointments'); 
        $router->post('appointment/fetch/archived','AppointmentController@fetchArchivedAppointments'); 
        $router->post('appointment/reschedule','AppointmentController@reschedule');
        $router->post('appointment/blocked/timeslots/create','AppointmentController@blockTimeslots');
        $router->post('appointment/blocked/timeslot/remove','AppointmentController@blockedTimeslotRemove');
        
        // ConfirmerController
        $router->post('confirmer/all','ConfirmerController@all');
        $router->post('confirmer/isapprover','ConfirmerController@userIsApprover');
        $router->post('confirmers/with/users','ConfirmerController@confirmersWithUsers');
        $router->post('confirmers/add','ConfirmerController@confirmerAdd');

        // CompanyController
        $router->post('company/fetch','CompanyController@fetch');

        // SubscriptionPlanOrderController
        $router->post('subscriptionplan/order/create','SubscriptionPlanOrderController@create');
        $router->post('subscriptionplan/order/edit','SubscriptionPlanOrderController@edit');
        $router->post('subscriptionplan/order/unpaid/all','SubscriptionPlanOrderController@allUnpaidFetch');
        $router->post('subscriptionplan/order/createdby','SubscriptionPlanOrderController@userWhoCreatedOrder');
         
        // SubscriptionplanController
        $router->post('subscription/forcompany/create','SubscriptionplanController@companySubscriptionCreate');
        $router->post('subscription/unused/assign/tocompany','SubscriptionplanController@assignUnusedSubscriptionPlanToCompany');
        
        // AttachmentController
        $router->post('attachmenttype/ofgroup/fetch','AttachmentController@attachmentTypeOfGroupFetch');
        $router->post('attachment/profile/add','AttachmentController@profileAttachmentAdd');
        $router->post('attachment/profile/edit','AttachmentController@profileAttachmentEdit');
        $router->post('attachment/profile/delete','AttachmentController@profileAttachmentDelete');
        $router->post('attachment/profile/ofuser','AttachmentController@profileAttachmentOfUser');

        // SaleController
        $router->post('sale/create','SaleController@create');
        $router->post('sale/edit','SaleController@edit');
        $router->post('sales/ofyear','SaleController@salesOfYear');
        $router->post('sales/ofagent/foryear','SaleController@salesOfAgentForYear');
        $router->post('sale/fetch','SaleController@saleDetails');

        // CustomerController
        $router->post('customer/create','CustomerController@create');
        $router->post('customer/edit','CustomerController@edit');
        $router->post('customer/fetchby/name','CustomerController@fetchByNameEmailPhone');
        $router->post('customer/ofcompany/fetch','CustomerController@fetchAllCustomersOfCompany');
        $router->post('customer/fetch','CustomerController@customerDetails');

        // CustomfieldController
        $router->post('customfield/formfields/fetch','CustomfieldController@fieldsOfFormFetch');
        $router->post('customfield/linkedtablerow/values/fetch','CustomfieldController@fieldValuesOfLinkedTableRowFetch');

        // CampaignController
        $router->post('campaign/ofcompany/fetch','CampaignController@campaignsOfCompanyFetch');

        // EmailCampaignController
        $router->post('emailcampaign/create','EmailcampaignController@create');
        $router->post('emailcampaign/fetch','EmailcampaignController@fetch');
        $router->post('emailcampaign/ofcompany/fetch/all','EmailcampaignController@fetchAllOfCompany');

        // EmailQueController
        $router->post('emailque/create','EmailQueController@create');

        // MailinglistCampaignController
        $router->post('mailinglist/create','MailinglistController@create');
        $router->post('mailinglistgroup/ofcompany/fetch/all','MailinglistController@fetchAllMailingGroupsOfCompany');
        $router->post('mailinglist/ofmailinggroup/fetch/all','MailinglistController@fetchAllMailingListOfMailingGroup');
    });
    
    // No access-token required for below routes
    $router->post('app/id','AppWhitelistController@fetchAppId'); 
    $router->post('subscriptionplan/fetch','SubscriptionplanController@fetch'); 

    // UserController
    $router->post('login','UserController@login');
    $router->post('logout','UserController@logout');
    $router->post('/userdetails', 'UserController@userdetails'); 
    $router->post('/accesstoken/byloginlogid', 'UserController@getAccessTokenUsingLoginLogId');  
    $router->post('/unverifieduser/create','UserController@unverifiedUserCreate');
    $router->post('/unverifieduser/email/verify','UserController@unverifiedUserEmailVerify');
    $router->post('/user/invited/verify','UserController@invitedUserVerify');
    $router->post('/user/invited/create','UserController@invitedUserCreate');

    // PasswordController
    $router->post('password/resetrequest/create','PasswordController@passwordResetRequestCreate');
    $router->post('password/reset/confirm','PasswordController@passwordResetConfirm');

    // EmailQueController
    $router->post('emailque/unsent/fetch','EmailQueController@fetchUnsentBatch');
    $router->post('emailque/marksent','EmailQueController@markAsSent');

    // MailConfigController
    $router->post('mailconfig/create','MailConfigController@create');
    $router->post('mailconfig/edit','MailConfigController@edit');
    $router->post('mailconfig/fetch','MailConfigController@fetch');
    $router->post('mailconfig/fetch/all','MailConfigController@fetchAll');
    $router->post('mailserver/change','MailConfigController@mailServerChange');
});

